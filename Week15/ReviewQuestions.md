Michael Wieland
256828

# Review Questions:

## R20: In the TLS record, there is a field for TLS sequence numbers. True or false?

False, it is part of the MAC calculation.

## R21: What is the purpose of the random nonces in the TLS handshake?

Nonces are used to create session keys which are then used to prevent connection replay attacks. Without these, an attacker could look identical to a client.

## R22: Suppose a TLS session employes a block cipher with CBC. True or false: The server sends to the client the IV in the clear.

True, the IV is sent in cleartext. The IV is a random k-bit string (initialization vector).

## R23: Suppose Bob initiates a TCP connection to Trudy who is pretending to be Alice. During the handshake, Trudy sends Bob Alice's certificate. In what step of the TLS handshake algorithm will Bob discover that he is not communicating with Alice?

Bob will discover this in Step 6 of the handshake. In this step, the computered HMAC will differ because when it is created with the MS, Trudy will not have access to ALice's private key.

# Problems section:

## P9: In this problem, we explore the Diffie-Hellman (DH) public-key encryption algorithm, which allows two entities to agree on a shared key. The DH algorithm makes use of a large prime number p and another large number g less than p. Both p and g are made public (so that an attacker would know them). In DH, Alice and Bob each independently choose secret keys, SA and SB, respectively. Alice then computes her public key, TA, by raising g to SA and then taking mod p. Bob similarly computes his own public key TB by raising g to SB and then taking mod p. Alice and Bob then exchange their public keys over the Internet. Alice then calculates the shared secret key S by raising TB to SA and then taking mod p. Similarly, Bob calculates the shared key S' by raising TA to SB and then taking mod p.

### a. Prove that, in general, Alice and Bob obtain the same symmetric key, that is, prove S = S'.

TA = g^SA mod p
TB = g^SB mod p
S = TB^SA mod p which is equivelent to g^(SB * SA) mod p
S' = TA^SB mod p which is eqivelent to g^(SA * SA) mod p

### b. With p = 11 and g = 2, suppose Alice and Bob choose private keys SA = 5 and SB = 12, respectively. Calculate Alice's and Bob's public keys, TA and TB. Show all work.

TA = g^SA mod p = 2^5 mod 11 = 10

TB = g^SB mod p = 2^12 mod 11 = 4

### c. Following up on part (b), now calculate S as the shared symmetric key. Show all work.

S = TB^SA mod p = 4^5 mod 11 = 1
S' = TA^SB mod p = 10^12 mod 11 = 1

### d. Provide a timing diagram that shows how Diffie-Hellman can be attacked by a man-in-the-middle. The timing diagram should have three vertical lines, one for Alice, one for Bob, and one for the attacker Trudy.

![Markdown Toggle](<RQP6d.png>)

## P14: The OSPF routing protocol uses a MAC rather than digital signatures to provide message integrity. Why do you think a MAC was chosen over digital signatures?

MAC's are inate to each device and are part of the hardware. You can easily figure out from and where data is transfered.

## P23: Consider the example in Figure 8.28. Suppose Trudy is a woman-in-the-middle, who can insert datagrams into the stream of datagrams going from R1 and R2. As part of a replay attack, Trudy sends a duplicate copy of one of the datagrams sent from R1 to R2. Will R2 decrypt the duplicate datagram and forward it to the branch-office network? If not, describe in detail how R2 detects the duplicate datagram.

R2 will not forward the datagram to the branch office and not decrypt it. It is able to detect the duplicate program because the R2 remembers the sequence numbers whihc are encrypted in the HMAC. When R2 checks this, it will see that the sequence numbers are out of order or have been tampered with, therefore it must be a duplicate datagram.