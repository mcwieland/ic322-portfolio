Michael Wieland
256828

## I can explain how two strangers are able to exchange secret keys in a public medium.

Alice wants to send a message to Bob without anyone else knowing the contents. The problem with doing this in a public medium is that everyone is able to see what is being sent. Additionally, we can assume that Alice and Bob have not previously talked to each other and exchanged a secret code between them already. 

Alice and Bob have both a public key (that everyone can view) and a private key (only they know). Alice must take the secret message and encrypt it with her private key, and then Bob's public key. When Bob recieves the message, he decrypts it with first his private key, and then Alice's public key. This is possible due to the nature of the encryption/decrption algorithm. Because Bob uses his private key to decrypt it, it can only be viewed by Bob and not be supseptable to intruders who are trying to listen in on the conversation.

## I can walk through the TLS handshake and explain why each step is necessary.

The TLS handshake functions similar to TCP, however does more to faciliate the secure exchange of keys and messages. The TLS handshake serves to prove that the person you are speaking to is actually who you think they are. It wants to prodeuce and share a master secret key that can be used to encrypt future conversations. There are 6 steps:

1. Client sends list of supported cryptograpic algorithms and client nonce.
    - To see which crypotographic algorithms are supported by both

2. Server chooses symmetric, public key, and HMAC algorithms from provided list and sends them back to the client as well as a certificate and server nonce.
    - To decide which algorithm to use

3. Client verifies certicate and then make the pre-master secret key (PMS) which it encrypts using the server's public key and then sends the PMS back to the server.
    - Encypts the master secret key and ensures the identity of the reciver by using the server's public key

4. Client and server idependantly produce the same master secret key (MS) using the key derivation function, PMS, and nonces. The MS is split to make two encryption and two HMAC keys along with two initializaton vectors. 
    - Because of this, all messages sent between client and server are encrypted and authenticated.

5. client sends the HMAC of all handshake messages.
6. server sends the HMAC of all handshake messages.

Steps 5 and 6 are for data integrity to make sure no data has changed during the transfer.

## I can explain how TLS prevents man-in-the-middle attacks.

Sequence numnbers are used extensively in order to prevent man in the middle attacks. The main job of sequence numbers in this situation is to make sure that no data is added or deleted before it gets to the reciever. Adding and deleting are easily discovered due to out or order sequence numbers. 

Additionally, it is hard to manipulate the data due to the HMAC (Hashed Mesage Authentication Code). The HMAc keeps track of a hased version of all the communication between the client and server. If something is changed, then the hash of the message will not match up the HMAC hashed record. 