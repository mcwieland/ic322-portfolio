Michael Wieland
256828

# Equifax data breach:

The Equifax data breach happened in March of 2017, when credit giant Equifax lost hundreds of millions of customer records. 


## Some background:

Equifax is a huge credit reporting agency who is responsible for assessing the financial health of many Americans. For their websites, they use an open source devlopment framework called Apache Struts. A vulnerbility within this system was discovered called CVE-2017-5638. On March 7th, Apache Software Foundation relased a patch for this vulnerbility and told all of their users to update it. And through a series of failures, Equifax did not update all of their sites properly or catch them. 


## Why was this possible?:

When Apache Software Foundation came out with their patch, the Equifax employee who was in charge of updating the systems didn't and the websites continued to use the unpatched version of Apache Structs. Additionally, the IT department ran a variety of scans to idenitfy any remaining unpatched systems, and the scans came up with nothing. The failure of these scans made everyone think that all the vulnerable systems were already patched, while in reality there were multiple vulnerable systems. The same month, security consulting firm Mandiant was hired to assess the Equifax systems and noted serveral unpatched and vulnerable systems. Equifax ignored them and did not fix any of the vulnerbilities that were found.

One of the vulnerabilities was a costuer complaint web portal, and was the first thing breached by the hackers. Once the hackers gained access to this, the atackers were able to move from system to system because of a lack of segmentation between them. Additionally, usernames and passwords were stored in plaintext within systems, which allowed access to even more systems. This whole time the attackers were able to extract data due to Equifax's failure to update an encryption certification. 

## The exploit:

Dubbed CVE-2017-5638, this exploit tricks the Struts into executing any code entered into the content-type header of an HTTP request. This potentially malicious code can be used to make Stucts run something that could open it up to further intrusion. It is an "easy to exploit vulnerability" and often is associated with the "multifile parser that is typically used for file uploads".

## Aftermath:

Equifax found out about the intrusion after they renewed their public key certification in 29 July, 2017. Before this, encrypted traffic wasn't able to be inspected, and the hackers had free reign to exfiltrate as much data as they pleased. One month after the data breach was out, Equifax publized it. In all, 143 million Americans were potentially affected. An additional 44 million British and 20 thousand Canadians were also compromised. Around 200 thousand had their credit card information tied to their personal information as well. In the majority of these cases, the names, addresses, dates of birth, social security numbers, and drivers licenese numbers were stolen, leading to potential identity theft, alothough no recorded cases origninated from this incident.

Equifax was faced with numerous lawsuits directly following the publicization of the data breah. Ultimently, they reached a deal with the FTC (Federal Trade Commission) and most state govermenets on 22 July 2019. This deal would call for 300 million for the victims, 175 million to the states and territories, and 100 million in fines. 

It is assumed that this incident was perpetrated by the Chinese. The US governemnt indicted 4 members of the Chinese on 9 charges relating to the hack. This is highly unusal to do, since it risks the Chinese doing the same to people on the American intelligence side. However, this underscores how important the US government belived the hack to be.


## Legal and Ethical Considerations:

### Ethical:

Ethically, Equifax did a poor job protecting their user's information. Their blantent security laspes caused the potential identity theft of almost 150 million Americans. Failing on multiple fronts, the security of Equifax was pitiful and easily could have avoided any incident if they paid closer attention. The lack of adequate protections on not only the data, but the system itself, left it open to be infiltrated through a already patched vulnerbility which leaves no reason that it could not have been handled. The company may have been trying to cut costs by understaffing its security departments, which only ended up hurting the company even more.

### Legal:

Legally, the 4 Chinese intelligence officers were charged with organiazing and leading the Equifax hack. Although, more people were defeinitely involved and were not charged. The group that orcastrated it has not been offically revealed. Equifax was forced to pay immense sums to both the affected costumers and fines to governments and organizations. Additionally, the company spent 1.4 billion on upgrading security after the incident to make sure nothing like that would happen again. 

### Professional Responibilities:

This was the largest data breach in scope and serverity ever. It affected over almost 200 million people from 3 countries and included immensly valuable information like social security numbers. It compromised all affected user's information and caused potential stress about indentity theft all over the country. 

Computer scientists are in charge of executing their jobs to their fullest extent and making sure it is know when they can't. Additionally they are in charge of maintaining system security at all times and don't leave themselves open to intruding parties. 

In this situation, thye failed in both of these areas. They did not execute their jobs properly, by not only not patching systems, not updating certificates, but also not immediatly publizing the data breah when found. Additionally, they did not adaquitly secure their systems. They used plaintext usernames and passwords as well as maining little to no segmentation to their systems, allowing intruders to gain access to all of them after gaining access to a singel one. 

Overall, there were multiple mishaps in Equifax's security plan, which were all taken advantage of to conduct the largest secirity breach in scale and severity in history.


## Resources used:

https://www.csoonline.com/article/567833/equifax-data-breach-faq-what-happened-who-was-affected-what-was-the-impact.html
https://www.ftc.gov/enforcement/refunds/equifax-data-breach-settlement
https://en.wikipedia.org/wiki/2017_Equifax_data_breach
https://isc.sans.edu/diary/22169
