Michael Wieland
256828

## I can explain the ALOHA and CSMA/CD protocols and how they solve the multiple-access problem.

Aloha and CSMA/CD are random access protocols. When a collison is detected in random access protocols, it is retransmitted after waiting a random amount of time. This process continues until the packet is successfully transmitted without a collision.

### Aloha:

The Aloha protocol has 2 main flavors **Unslotted** and **Slotted**.

**Unslotted**: Each frame is transmitted over a broadcast channel. If there is a collision, then the sending node will retransmitted at propability p. Or otherwise the node retransmitts after the time of probability p. 

**Slotted**: All frames in slotted Aloha are L bits long. Time must be divides into slots of size L/R (time is the time that it takes to transmit one frame). If there is a collision, then a retransmisison will reoccur at probability p at the beginning of each time slot.


Unslotted Aloha has an efficent factor of around 0.185 and slotted Aloha has a efficency of 0.3.
### CSMA/CD: 

This is another random access protocol called Carrier Sense Multiple Access / with Collision Detection. This protocol revolves around the strategy of only transmitting whenever the broadcast channel is empty. An example of this can be found in the real world. Regular human conversations happen with only one side speaking at a time, and the other can start speaking after the first finishes. The collision detection part of this implements "carrier sensing" This is where a node is able to monitor a broadcast channel and see if something is being broadcasted over it at that time. This happens at a random interval absed off probability p. Because of propagation delay, it is possible for carrier sensing to think that the channel is empty while it is actually being used by the other node, leading to collisons.

*
## I can compare and contrast various error correction and detection schemes, including parity bits, 2D parity, the Internet Checksum, and CRC.

### Parity Bits:
The most common implementation of this is an even parity scheme. When information is sent in bits, an additional bit called "the parity bit" can be sent along with the data. This bit can either be a 0 or 1 depending on the other bits that it is sent with. If the sum of the other bits are odd, then the parity bit will be a 1 in order to make ths combined sum of all of them even. If the sum of the other bits are even, then the parity bit will be 0 to retain the even characteristic of the data. This can be used to error check because we know that the combined sum of the bits must be even, so if there is a situation where they are odd, we know there must have been an error.

### 2D Partity:
This scheme combines parity bits and a 2D matrix. Essentially, this strategy takes a 2D matrix of data and performs an even parity scheme on each individual row and column. This results in a full row and column of parity bits. To error check, we error check each row and column seperately, noting each error row and column that we find. We can then cross reference these and find the exact bit that was flipped, in most situations.

### The Internet Checksum:
The Internet checksum is an error checking scheme where you split up the data into k-bit integers and add them together. The 1s complement of the resulting number is entered into the segment header. This can be used to check for errors, beacuse the reciever can take the 1s complement of the data with the checksum. A reult of 1 indicates an error, while a 0 does not.

### CRC:
This scheme is called cyclic redundancy check. It is used commonly today. It basically takes some data d that which one node wishes to send to another as well as a generator bit (R+1). The generator bit always starts with a 1. R amount of bits are appended at the end. This new number is divided by G and made sure that there is no remainder. If there is, an error must be present.

## I can describe the Ethernet protocol including how it implements each of the Layer 2 services and how different versions of Ethernet differ.

Ethernet is a wired LAN technology that exists in the link layer rather than the network layer. Because of this, Ethernet uses link layer addresses, MAC addresses. It encapsulates the IP datagram within an ethernet frame and passes the frame to the physical layer in the form of ethernet cords. The reciver passes it to the network layer after recieving the IP datagram. Orginally, ethernet started as a hub-based technology, however it eventually changed over to being controled by switches.

Ethernet versions are denoted by different acronyms. In an example 1000Base-T, 1000 denotes a speed of 1000 Gigabits per second, BASE means only ethernet traffic is carried across that wire, T means that the type of medium is a twisted-pair copper wire. 