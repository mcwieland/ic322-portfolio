Michael Wieland
256828

# Review Questions: 

## R4: Suppose two nodes start to transmit at the same time a packet of length L over a broadcast channel of rate R. Denote the propagation delay between the two nodes as dprop. Will there be a collision if dprop < L/R? Why or why not?

Yes, if dprop is L/R because the first packet will not be fully sent across the wire when the same node starts to read in the frame sent to it by the other node.

## R5: In Section 6.3, we listed four desirable characteristics of a broadcast channel. Which of these characteristics does slotted ALOHA have? Which of these characteristics does token passing have?

1) When only one node has data to send, that node has a throughput of R bps.
2) When M nodes have data to send, each of these nodes has a throughput of R/M bps. This need not necessarily imply that each of the M nodes always has an instantaneous rate of R/M, but rather that each node should have an average transmission rate of R/M over some suitably defined interval of time.
3) The protocol is decentralized; that is, there is no master node that represents a single point of failure for the network.
4) The protocol is simple, so that it is inexpensive to implement.

Slotted Aloha has all 4 of these. 
Tocken Passing has 1, 2, and 4. It also is deentralized making it reliant on the functionality of the entire network.


## R6: In CSMA/CD, after the fifth collision, what is the probability that a node chooses K=4? The result K=4 corresponds to a delay of how many seconds on a 10 Mbps Ethernet?

The probability of a node choosing k=4 is 1/2^n. Which is 1/32. This will be (K * 512) / 10 microseconds, which comes out to 204.8 microseconds.

# Problems section: 

## P1: Suppose the information content of a packet is the bit pattern 1110 0110 1001 0101 and an even parity scheme is being used. What would the value of the field containing the parity bits be for the case of a two-dimensional parity scheme? Your answer should be such that a minimum-length checksum field is used.


1 |1 |1 |0| 1
--|--|--|-|--
0 |1 |1 |0| 0
1 |0 |0 |1| 0
0 |1 |0 |1| 0
0 |1 |0 |0| 1

## P3: Suppose the information portion of a packet (D in Figure 6.3) contains 10 bytes consisting of the 8-bit unsigned binary ASCII representation of string “Internet.” Compute the Internet checksum for this data.

73, 110, 116, 101, 114, 110, 101, 116, 46
Convert to binary and add, take the 1's complement of answer.

Sum: 01110111 -> Checksum: 10001000 

## P6: Consider the previous problem, but suppose that D has the value

### a. 1000100101.

R = 549mod(19) = 17 -> 10001

### b. 0101101010.

R = 362mod(19) = 1

### c. 0110100011.

R = 419mod(19) = 1

## P11: What is the efficiency of this four-node system?

### a. What is the probability that node A succeeds for the first time in slot 4?

(1 - p)^3 * p * (1-p)^3

### b. What is the probability that some node (either A, B, C or D) succeeds in slot 5?

p*4 * (1/4)

### c.  What is the probability that the first success occurs in slot 4?

4 * ((1-p)^3)^4 * 4

### d. What is the efficiency of this four-node system?

4p(1-p)^3

## P13: Consider a broadcast channel with N nodes and a transmission rate of R bps. Suppose the broadcast channel uses polling (with an additional polling node) for mulitple access. Suppose the amount of time from when a node completes transmission until the subsequent node is permitted to transmit (that is, the polling delay) is dpoll. Suppose that within a polling round, a given node is allowed to transmit at most Q bits. What is the maximum throughput of the broadcast channel?

The maximum throughput of the broadcast channel wouls be Q / ((R/Q) + dpoll)