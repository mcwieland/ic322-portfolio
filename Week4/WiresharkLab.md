# WIRESHARK LAB WEEK 4
Michael Wieland
Help Recieved: None

## Introduction
This lab introduced us to TCP through the use of HTTP. We uploaded large text documents and used sequence numbers and acknologments to analyze the capabilities and features of TCP.

## Process:
I was able to follow the online steps at https://gaia.cs.umass.edu/kurose_ross/wireshark.php-. All of my questions and answers for Wireshark Lab 4, TCP, Version 8.1 are below.

## Questions:
### 1 What is the IP address and TCP port number used by the client computer (source) that is transferring the alice.txt file to gaia.cs.umass.edu?  To answer this question, it’s probably easiest to select an HTTP message and explore the details of the TCP packet used to carry this HTTP message, using the “details of the selected packet header window” (refer to Figure 2 in the “Getting Started with Wireshark” Lab if you’re uncertain about the Wireshark windows).
The IP of the source computer is 192.168.86.68 and its port number is 55639.

### 2 What is the IP address of gaia.cs.umass.edu? On what port number is it sending and receiving TCP segments for this connection?
The IP address of gaia.cs.umass.edu is 128.119.245.12. The port number it is using for TCP is 80.

### 3 What is the sequence number of the TCP SYN segment that is used to initiate the TCP connection between the client computer and gaia.cs.umass.edu? (Note: this is the “raw” sequence number carried in the TCP segment itself; it is NOT the packet # in the “No.” column in the Wireshark window.  Remember there is no such thing as a “packet number” in TCP or UDP; as you know, there are sequence numbers in TCP and that’s what we’re after here.  Also note that this is not the relative sequence number with respect to the starting sequence number of this TCP session.). What is it in this TCP segment that identifies the segment as a SYN segment? Will the TCP receiver in this session be able to use Selective Acknowledgments (allowing TCP to function a bit more like a “selective repeat” receiver, see section 3.4.5 in the text)?
The sequence number of the TCP segment is 4236649187. Since the SYN flag is set to 1, it means that this segment is a SYN segment. The TCP reciever will be able to use selective acknowledgements because the TCP Option is SACK permitted.

### 4 What is the sequence number of the SYNACK segment sent by gaia.cs.umass.edu to the client computer in reply to the SYN? What is it in the segment that identifies the segment as a SYNACK segment? What is the value of the Acknowledgement field in the SYNACK segment? How did gaia.cs.umass.edu determine that value?
The sequence number is 1068969752. In the flags portion of TCP, both flags ACK and SYN are set making is a SYNACK segment. The value of 1 was determined because it is an ACK response to teh client's SYN message.

### 5 What is the sequence number of the TCP segment containing the header of the HTTP POST command?  Note that in order to find the POST message header, you’ll need to dig into the packet content field at the bottom of the Wireshark window, looking for a segment with the ASCII text “POST” within its DATA field,.  How many bytes of data are contained in the payload (data) field of this TCP segment? Did all of the data in the transferred file alice.txt fit into this single segment?
The sequence number of the the HTTP POST segment is 4236649188. The number of bytes included in this segment is 16427 bytes. Not all of the data of alice.txt was able to fit. We can see this in the Reassembled TCP segments field.

### 6 Consider the TCP segment containing the HTTP “POST” as the first segment in the data transfer part of the TCP connection.

### a At what time was the first segment (the one containing the HTTP POST) in the data-transfer part of the TCP connection sent?
The time was 21:43:26.71692200.

### b At what time was the ACK for this first data-containing segment received?
It was received 21:43:26.74554600.

### c What is the RTT for this first data-containing segment?
The RTT for this segment was 21:43:26.745546000 - 21:43:26.716922000 = 0.028624 seconds.

### d What is the RTT value the second data-carrying TCP segment and its ACK?
21:43:26.74555100 - 21:43:26.71692300 = 0.028628 seconds.

### e What is the EstimatedRTT value (see Section 3.5.3, in the text) after the ACK for the second data-carrying segment is received? Assume that in making this calculation after the received of the ACK for the second segment, that the initial value of EstimatedRTT is equal to the measured RTT for the first segment, and then is computed using the EstimatedRTT equation on page 242, and a value of α = 0.125.
(1-a) * ERTT + a * SampleRTT = 
(1-.125) * 0.028624 + 0.125 * 0.028628 = 0.02864025 seconds.

### 7 What is the length (header plus payload) of each of the first four data-carrying TCP segments?
All segments are 1448 of payload with 32 bytes for their header each.

### 8 What is the minimum amount of available buffer space advertised to the client by gaia.cs.umass.edu among these first four data-carrying TCP segments?  Does the lack of receiver buffer space ever throttle the sender for these first four data-carrying segments?
The minimum amount of buffer space advertised is 131712 bytes. The sender is never throttled for lack of reciever buffer space, because it never reaches the maximum.

### 9 Are there any retransmitted segments in the trace file? What did you check for (in the trace) in order to answer this question?
No, there were not any retransmitted segments. You can look for duplicate ACKs or repeat sequence numbers.

### 10 How much data does the receiver typically acknowledge in an ACK among the first ten data-carrying segments sent from the client to gaia.cs.umass.edu?  Can you identify cases where the receiver is ACKing every other received segment (see Table 3.2 in the text) among these first ten data-carrying segments?
It looks like it the receiver typically acknoledges around 1480 bytes per segment. The reciever does not ACK every other recieved segment within the first 10 data-carrying segments.

### 11 What is the throughput (bytes transferred per unit time) for the TCP connection?  Explain how you calculated this value.
1480 bytes / 0.02864025 seconds = 51,676 bytes/second
I used the length of the TCP segments and the EstimatedRTT value.

### 12 Use the Time-Sequence-Graph(Stevens) plotting tool to view the sequence number versus time plot of segments being sent from the client to the gaia.cs.umass.edu server.  Consider the “fleets” of packets sent around t = 0.025, t = 0.053, t = 0.082 and t = 0.1. Comment on whether this looks as if TCP is in its slow start phase, congestion avoidance phase or some other phase. Figure 6 shows a slightly different view of this data.

It looks like TCP is in the slow start phase. This makes sense on the graph because of the doubling of the size of sequence numbers per fleet. 

### -13 These “fleets” of segments appear to have some periodicity. What can you say about the period?
The period is about .025 seconds.