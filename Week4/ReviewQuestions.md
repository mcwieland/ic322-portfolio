Michael Wieland
256828


Review Questions:


R5: Why is it that voice and video traffic is often sent over TCP rather than UDP in today’s Internet? (Hint: The answer we are looking for has nothing to do with TCP’s congestion-control mechanism.)


Orginally, voice and video traffic has used UDP protocols in order to transmit over the internet. This is because voice and video traffic does not need the reliability that comes from TCP and instead perfers speed. A few dropped packets doesn't mean much in terms of loading a video. However, because we watch videos over browsers now, it is more common to see voice and video traffic over TCP because that is what the browsers use. 


R7: Suppose a process in Host C has a UDP socket with port number 6789. Suppose both Host A and Host B each send a UDP segment to Host C with destination port number 6789. Will both of these segments be directed to the same socket at Host C? If so, how will the process at Host C know that these two segments originated from two different hosts?


At host C, the same socket will accept both segments from host A and host B. This is because UDP sockets are defined by a two-tuple of the destination IP address and the destination port number. The process at host C will be able to deteremin that they came from different hosts because the source IP address and port are included as well with the packet.


R8: Suppose that a Web server runs in Host C on port 80. Suppose this Web server uses persistent connections, and is currently receiving requests from two different Hosts, A and B. Are all of the requests being sent through the same socket at Host C? If they are being passed through different sockets, do both of the sockets have port 80? Discuss and explain.


If they are connecting through Port 80, that means that they are connecting over a TCP connection, HTTP uses TCP. TCP has a welcome socket for every port number. This socket accepts the connection from host A and creates a new socket at port 80 with a four-tuple consisting of the source IP and port and the destination IP and port. After this is created, the welcome socket accepts and creates another socket speciifcially for the connection between it and host B. This unique aspect of TCP connections, the fact they have a designated socket just for thier connection, improves the reliabilty of TCP connections greatly. In this situation 3 sockets are being used, 2 for specific connections and 1 welcoming, and they are all connected to port 80.


R9: In our rdt protocols, why did we need to introduce sequence numbers?


Sequence numbers are used in order to tell if we are in a situation where a packet does not make it to the recieving host, being dropped or corrupted along the way. Using sequence numbers we are able to differeciate between packets and point out which ones are ok and which ones need to be retransmitted because they were dropped. This is a fundimental part of reliable data transfer.


R10: In our rdt protocols, why did we need to introduce timers?


Timers are a solution to corrupted or lost packets, especially acks. A timer tracks the time that it takes to send a packet and get an acknologment response back from the recieving host. There is a timeframe built into the system where if we don't get an ack in time, we would consider the packet lost and resend it.


R11: Suppose that the roundtrip delay between sender and receiver is constant and known to the sender. Would a timer still be necessary in protocol rdt 3.0, assuming that packets can be lost? Explain.


A timer will still be nessisary becauase there are other delays besides the roundtrip delay that a packet can experience. It will greatly narrow the time frame that which we can declare it lost because we know when about it should return an acknologment, however, it is not exact.


R15: Suppose Host A sends two TCP segments back to back to Host B over a TCP connection. The first segment has sequence number 90; the second has sequence number 110.


a: How much data is in the first segment?


There are 20 byes in the first segment. 110 - 90 = 20.


b: Suppose that the first segment is lost but the second segment arrives at B. In the acknowledgment that Host B sends to Host A, what will be the acknowledgment number?


It would be ACK 90 because host B will respond with the sequence number that it is expecting. In this case it is the first segment because it has not arrived yet.


Problems section: 


P3: UDP and TCP use 1s complement for their checksums. Suppose you have the following three 8-bit bytes: 01010011, 01100110, 01110100. What is the 1s complement of the sum of these 8-bit bytes? (Note that although UDP and TCP use 16-bit words in computing the checksum, for this problem you are being asked to consider 8-bit sums.) Show all work. Why is it that UDP takes the 1s complement of the sum; that is, why not just use the sum? With the 1s complement scheme, how does the receiver detect errors? Is it possible that a 1-bit error will go undetected? How about a 2-bit error?


01010011 + 01100110 = 10111001
10111001 + 01110100 = 00101110

00101110 -> 11010001

The reciever adds 4 words together, one of them being the checksum. If there are any "0"s in the result, then the reciever knows that there was an error somewhere. This system makes it so 1-bit errors will never go undetected, but 2-bit errors possibily could.