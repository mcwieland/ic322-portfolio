I can explain how TCP and UDP multiplex messages between processes using sockets.


TCP and UDP are transport level protocols that are used to transmit data packets. They use sockets and their accociated ports to make sure that packets are transmitted properly. Multiplexing speeds up this process of data transmitting by concurrently using different sockets (connected to differnt ports) in order to create multiple routes for data to be transfered. Transport layer multiplexing requires that sockets have unique identifiers and that each segment has special fields that have the source port and destination port. The ports 0 - 1023 are "well known port numbers" and cannot usally be used. However, ports 1024-65535 usally can be used. 

UDP (User Datagram Protocol): The transport layer automatically assigns a port number 0-65535 that is not currently being used by UDP. It then creates a transport layer segment with the application data, source port number, and destination port number and sends to to the network layer to be encapsulated in an IP datagram which is sent with a best-effort attempt to deliver the segment to the reciving host. Once at the recieving host, the datagram is connected to the socket associated with the given destination port number which is called demultiplexing. UDP sockets are described by a two-tuple of a destination IP adddress and destination port.

TCP(Transmission Control Protocol): TCP sockets are described by a four tuple of the source IP address, source port number, destination IP address, and destination port number. The reciving host uses these four values to demultiplex the segment to the appropriate socket. One difference between this and UDP is if two TCP segments with different source IP addresses will be directed to two differnt sockets on the recieving host's side. Besides this, TCP multiplexing functions in a similar way to UDP.


I can explain the difference between TCP and UDP, including the services they provide and scenarios each is better suited to.


TCP(Transmission Control Protocol): It provides a reliable and in-order deliveer of packets. It ensures that packets are sent corectly without any dropped packets or repetition. It also uses a three-way handshake to make sure that a reiable connection is established beofre sending packets. It is able to manage congestion through a mechanism that throttles the TCP sender when the links between the source and destination become excessivly congested. TCP is also able to error check and correct missing packets by continously sending a segment until a recipt is sent back. TCP is best used in situations such as email and websites.

UDP(User Datagram Protocol): Unlike TCP, UDP does not offer garenteed reliablity of delivery or ordering. This is because it is a connectionless protocol which does not provide a three way handshake like TCP. It also does not have any congestion-control mechanisms making it possible for the system to be overloaded with data. Because of its streamlined design, it has a lower overhead and can be run at a much faster rate, taking on many more clients than a TCP connection can handle. UDP is best used in situations where reiablity is not the most important thing such a video loading, which needs to be fast.


I can explain how and why the following mechanisms are used and which are used in TCP: sequence numbers, duplicate ACKs, timers, pipelining, go-back-N, selective repeat, sliding window.


Sequence numbers: These are used to number every byte of a segment in order to ensure that they can be reassembled in order when they reach the destination host. This is one of the main ways that TCP is able to ensure relaible data transfer.

Duplicate ACKs: These are sent by the reciever in order to indicate that they have recievd packets that are out of order. They are used in TCP connections and use sequence numbers to indentify if this has happened. It is used to prevent packet loss and let the sender know to retransmit the segment.

Timers: Tracks the length of time that it takes for an ack after sending a packet. If an ack is recieved, the time resets for the next packet sent. This is used to make sure packets are delivered. If a packet times out, the sender attempts to send it again. Used in TCP connections.

Pipelining: Allows for concurrent connections that allows multiple packets to be sent at the same time. This reduces congestion on busy ports and increases the speed of transmission gretaly. Used by TCP connections.

Go-Back-N: This is an alternative to the protocol that TCP uses. In this system, multiple packets in a "window" are transmitted at once without needed to wait for acks. This increases speed as no acks are needed to be waited upon. However, if a packet is dropped, it can be ineffecient since most of the window must be retransmitted. Not used by TCP.

Selective Repeat: This is a protocol used by TCP that allows out of order packets to be transmitted. Selective repeat only sends teh missing packets, unlike Go-Back-N where it resends even successfully send packets again. Because of this, it is able to cut corners that Go-Back-N doesn't, giving it more effiency and speed.

Sliding Windows: This is used in TCP as well at Go-Back-N protocols. It is used to limit the number of packets that are sent at once based on the desired "window size". It controls the rate of transmission of packets as well as making sure that the only a small portion of the data sent is corrupted from a missing packet.


I can explain how these mechanisms are used to solve dropped packets, out-of-order packets, corrupted/dropped acknowledgements, and duplicate packets/acknowledgements.


Sequence numbers are used to tell what order packets are sent in. This makes them particularly useful in maintaining the integrity of the "in-order" aspect of TCP. These sequence numbers are used by the reciveing host to send accurate acks to the sender and therefore to let them know what packets have actually arrived. Based on the protocol used by TCP, selective repeat or Go-Back-N, this is identified and handled differently. Packets corrupted/dropped during transmission are found out by timer which times how long it takes for the packet to be sent and recieve an ack. If a packet times out, it is retranmitted based on the system that the protocol being used takes. Duplicate packets are detected based on the acks sent by the recieving host to the sender and specifically the ack sequence number that comes along with it. In the case of dupicate acks, the requested packet is retransmitted and possibly more depending on the protocol used.

When a packet is dropped in the Go-Back-N protocol, all the next packets must be retransmitted. In this case, duplicate acks are recieved. When a packet is dropped in the selective repeat protocol, only the missing packet is retransmitted, making data correction much faster when compared to Go-Back-N.