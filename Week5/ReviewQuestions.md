Michael Wieland
256828


# Review Questions: #


## R17: Suppose two TCP connections are present over some bottleneck link of rate R  bps. Both connections have a huge file to send (in the same direction over the  bottleneck link). The transmissions of the files start at the same time. What  transmission rate would TCP like to give to each of the connections?


TCP would like give a transmission rate of R/K where R is the total transmission rate and K is the number of TCP connections. In this situation, the transmission rate would be R/2 because the total transmission is split between 2 connections.


## R18: True or false? Consider congestion control in TCP. When the timer expires at the sender, the value of ssthresh is set to one half of its previous value.


False, the value of ssthresh is set to half of cwnd (the congestion window).


# Problems section: 


## P27: Host A and B are communicating over a TCP connection following RFC 5681. Host B has already received from A all bytes up through byte 96. Suppose Host A then sends two segments to Host B back-to-back. The first and  the second segments contain 40 and 80 bytes of data, respectively. In the first segment, the sequence number is 127, the source port number is 302, and the destination port number is 80. Host B sends an acknowledgment whenever it receives a segment from Host A.


### a. In the second segment sent from Host A to B, what are the sequence number, source port number, and destination port number?


The sequence number would be 207, the source port number 302, and the destination port 80.


### b. If the first segment arrives before the second segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number,  the source port number, and the destination port number?


The acknowledgment number would be 207, source port, 80 and detination port 302.


### c. If the second segment arrives before the first segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number?


The acknowledgemnt number would be 127.


### d. Suppose the two segments sent by A arrive in order at B. The first  acknowledgment is lost and the second acknowledgment arrives after the  first timeout interval. Draw a timing diagram, showing these segments  and all other segments and acknowledgments sent. (Assume there is no  additional packet loss.) For each segment in your figure, provide the  sequence number and the number of bytes of data; for each acknowledgment that you add, provide the acknowledgment number.


(Timing diagram in Week5 folder)


## P33: In Section 3.5.3, we discussed TCP’s estimation of RTT. Why do you think TCP avoids measuring the SampleRTT for retransmitted segments?


TCP might not want to measure the SampleRTT of retransmitted segments because we could not tell for sure if the RTT that we recieve was from the orginal or the retransmitted packet. If the ACK response comes arrives right after the host retransmitted, the measured RTT would be inacurate.


## P36: In Section 3.5.4, we saw that TCP waits until it has received three duplicate ACKs before performing a fast retransmit. Why do you think the TCP designers chose not to perform a fast retransmit after the first duplicate ACK for a segment is received?


TCP designers were probably just accounting for the possibility of out-of-order packets. Just because packets are out of order doesn't nessisarly mean that they are lost. The path from one host to another is not always constant, therefore some out-of-order packets are just slow and will arrive at the recieving host moments after the packet sent after it does. In order to account for this, the TCP designers might have decided that the chances of a packet being out-of-order and still arriving successfully are so low that it is more time effiecent to call it lost. 


## P40: Consider Figure 3.61. Assuming TCP Reno is the protocol experiencing the behavior shown above, answer the following questions. In all cases, you should provide a short discussion justifying your answer.
### a. Identify the intervals of time when TCP slow start is operating.


Times 1-6 and 23-26. Here the cwnd is increasing exponentially.


### b. Identify the intervals of time when TCP congestion avoidance is operating.


Times 6-16 and 17-22. The cwnd is increasing linearly.


### c. After the 16th transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?


Triple duplicate ACK, the cwnd is halfed.


### d. After the 22nd transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?


Timeout. The cwnd resets at 0.


### e. What is the initial value of ssthresh at the first transmission round?


The initial value of ssthresh is 32 because this is the size when the graph started to increase linearly.


### f. What is the value of ssthresh at the 22nd transmission round?


The value of ssthresh would be 21, have of 42 (when triple ack was found).


### g. During what transmission round is the 70th segment sent?


It is sent around round 7. Take the integral of the graph.


### h. Assuming a packet loss is detected after the 26th round by the receipt of a triple duplicate ACK, what will be the values of the congestion window size and of ssthresh?


The cwnd is around 8 and ssthresh would be 4 because it is halfed.


### i. Suppose TCP Tahoe is used (instead of TCP Reno), and assume that triple duplicate ACKs are received at the 10th round. What are the ssthresh and the congestion window size at the 11th round?


The cwnd will drop to 0 because of the triple ack. The ssthresh will be 21.


### j. Again, suppose TCP Tahoe is used, and there is a timeout event at the 22nd round. How many packets have been sent out from the 17th round till the 22nd round, inclusive?


total packets = 1 + 2 + 4 + 8 + 16 + 32 = 63 packets.