Michael Wieland
256828

# Review Questions: 

## R11: How does BGP use the NEXT-HOP attribute? How does it use the AS-PATH attribute?

AS-Path is an attibute that contains a list of all the ASs that an advertisement passes through. This help prevents duplicate ASs because a router can look to see if ASN is already part of the list. If it is, the advertisment is rejected.

The Next-Hop attiribe is the IP address of the router that is first in the AS-Path list. This helps start the route to a specific IP prefix.

## R13:  True or false: When a BGP router receives an advertised path from its neighbor, it must add its own identity to the received path and then send that new path on to all of its neighbors. Explain.

This is false because if a BGP router is already on the AS-Path list, then it must reject the advertisement entirely. However, most of the time this holds true.

## R19 Names four different types of ICMP messages (also describe what they are used for)

Type 0:: echo reply, used to reply to ping requests (Type 8).
Type 3: destination network unreachable if a router is unable to resolve an IP
Type 8: echo request, used to determien if a target system is responsive
Type 10: Router discovery, used to find operational routers on a subnet

## R20: What two types of ICMP messages are received at the sending host executing the traceroute program?

Type 11 (TTL) is used to tell of the TTL has expired. This give suse th RTT and the Ip address of the router that it came from. Type 3 (port unreachable) is used to tell us if the port selected is unreachable or not. This will tell the sender to stop sending packets to the port.

# Problems Section:

## P14 Consider the network shown below. Suppose AS3 and AS2 are running OSPF for their intra-AS routing protocol. Suppose AS1 and AS4 are running RIP for their intra-AS routing protocol. Suppose eBGP and iBGP are used for the inter-AS routing protocol. Initially suppose there is no physical link between AS2 and AS4.

### a: Router 3c learns about prefix x from which routing protocol: OSPF, RIP, eBGP, or iBGP?

eBGP

### b: Router 1c learns about x from which routing protocol?

OSPF

### c: Router 1c learns about x from which routing protocol?

eBGP

### d: Router 1d learns about x from which routing protocol?

RIP

## P15 Referring to the previous problem, once router 1d learns about x it will put an entry (x, I) in its forwarding table.

### a: Will I be equal to I1  or I2  for this entry? Explain why in one sentence.

I1 because it is the only path with a physical link to x.

### b: Now suppose that there is a physical link between AS2 and AS4, shown by the dotted line. Suppose router 1d learns that x is accessible via AS2 as well as AS3. Will I be set to I 1  or I 2 ? Explain in one sentence.

I2 because it is the shortest path to x.

### c: Now suppose there is another AS, called AS5, which lies on the path between AS2 and AS4 (not shown in diagram). Suppose router 1d learns that x is accessible via AS2 AS5 ASS4 as well as via AS3 AS4. Will I be set to I1 or I2? Explain why in one sentence.

I1 because it takes the least number of ASs to get to 1d.

## P19 In Figure 5.13, suppose that there is another stub network V that is a customer of ISP A. Suppose that B and C have a peering relationship, and A is a customer of both B and C. Suppose that A would like to have the traffic destined to W to come from B only, and the traffic destined to V from either B or C. How should A advertise its routes to B and C? What AS routes does C receive?

A should advertise to B that it is connected to both W and V. To C, A should advertise that it is only connected to V. C receives an AS route to V from A, but not to W.

## P20: Suppose ASs X and Z are not directly connected but instead are connected by AS Y. Further suppose that X has a peering agreement with Y, and that Y has a peering agreement with Z. Finally, suppose that Z wants to transit all of Y's traffic but does not want to transits X's traffic. Does BGP allow Z to implement this policy?

If X peers with Y and Y peers with Z, X has a route to peer with Z through Y. BGP can get reachability info from neighboring networks, so it will allow the implemetation of the policy.