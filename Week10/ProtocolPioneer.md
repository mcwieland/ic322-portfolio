Michael Wieland 
256828

# Introduction

I chose to do a loopbacker assignment. I went back to complete the Protocol Pioneer Chapters 3 and 4. The purpose of this is to beta test the game and also gain a better understanding of computer networks.

# Collaboration

LCDR Down's solutions are available and were part of the game when I copied it over.

# Process

Protocol Pioneer Chapters 3 and 4

## Strategy:

Referenced LCDR DOwn's given solution when stuck. One idea for this was to make tons of if statements for each individual drone to "hard code" each drone to ehe correct position. The strategy that LCDR Downs went for is muhc more elegent and applicable to this class. There would be a forwarding table for each drone whihc directs each drone to the correct position. This made it so every message could get exactly where they needed to go just by looking at teh directions from the tables.

### Chapter 3:
        routing_table = [
            [".", "E", "N", "N", "E", "N"], # Drone 1 forwarding directions. 
            ["W", ".", "W", "N", "S", "N"], # Drone 2 forwarding directions.
            ["S", "E", ".", "E", "E", "E"], # Drone 3 forwarding directions. 
            ["W", "S", "W", ".", "S", "N"], # Drone 4 forwarding directions. 
            ["N", "N", "N", "N", ".", "N"], # Drone 5 forwarding directions. 
            ["S", "S", "S", "S", "S", "."], # Drone 6 forwarding directions. 
            ]

        cmds = {
            "start_emit": self.start_emit, # method for starting emission. 
            "focus": self.focus, # method for focus.
            "return_results": self.return_results # method for returning results. 
        }
        
        while(self.message_queue):
            m = self.message_queue.pop()
            # You can use the `self.id` value to create different logic for different drones.
            print(f"--- Drone {self.id}: Msg on interface {m.interface} ---\n{m.text}\n------------------")
            splitted = m.text.replace("\n",":").split(":") # python method to split strings. 
            _, dest, _, cmd, _, value = splitted # setting the values to each variable, ignoring the info we don't need.
            interface = routing_table[int(self.id)-1][int(dest)-1]
            if interface == ".":
                cmds[cmd](value)
            else:
                self.send_message(m.text, interface)


There is a similar approach with this problem. Again, there is a forwarding table for each drone. Every drome, except the 2.1 drone (which only has 1 route) has a forwarding table that can direct the messages through the drones along the correct paths. The forearding tables get rid of the need of tons of if statements which was another possible strategy. This one was much more efficent. 

### Chapter 4:
        routing_table = {
        "3.1": {"1.1": "W", "1.2": "S", "1.3": "S",
                "2.1": "E", "2.2": "S",
                "3.1": ".", "3.2": "S", "3.3": "S",
                "3.4": "S", "3.5": "S", "3.6": "E",
                "3.7": "E",
        },
        "3.2": {"1.1": "N", "1.2": "W", "1.3": "S",
                "2.1": "N", "2.2": "S",
                "3.1": "N", "3.2": ".", "3.3": "S",
                "3.4": "E", "3.5": "E", "3.6": "E",
                "3.7": "E",
        },
        "3.3": {"1.1": "N", "1.2": "N", "1.3": "S",
                "2.1": "N", "2.2": "E",
                "3.1": "N", "3.2": "N", "3.3": ".",
                "3.4": "S", "3.5": "E", "3.6": "N",
                "3.7": "N",
        },
        "3.4": {"1.1": "N", "1.2": "N", "1.3": "W",
                "2.1": "E", "2.2": "N",
                "3.1": "N", "3.2": "N", "3.3": "N",
                "3.4": "N", "3.5": "N", "3.6": "N",
                "3.7": "N",
        },
        "3.5": {"1.1": "W", "1.2": "W", "1.3": "W",
                "2.1": "N", "2.2": "S", 
                "3.1": "N", "3.2": "N", "3.3": "W",
                "3.4": "W", "3.5": ".", "3.6": "N",
                "3.7": "N",
        },
        "3.6": {"1.1": "W", "1.2": "W", "1.3": "W",
                "2.1": "N", "2.2": "S",
                "3.1": "W", "3.2": "W", "3.3": "W",
                "3.4": "W", "3.5": "S", "3.6": ".", 
                "3.7": "N",
        },
        "3.7": {"1.1": "W", "1.2": "S", "1.3": "S",
                "2.1": "N", "2.2": "S",
                "3.1": "W", "3.2": "S", "3.3": "S",
                "3.4": "S", "3.5": "S", "3.6": "S",
                "3.7": ".",
        }
    }

    def drone_strategy(self):
        """Drones are responsible for routing messages."""
        while(self.message_queue):
            m = self.message_queue.pop()
            print(f"--- Drone {self.id}: Msg on interface {m.interface} ---\n{m.text}\n------------------")
            splitted = m.text.replace("\n",":").split(":")
            _, source, _, dest, _, cmd, _, value = splitted
            interface = routing_table.get(self.id).get(dest)
            self.send_message(m.text, interface)

    def scanner_strategy(self):
        """Scanners are responsible for receiving messages, parsing them, taking
        action, and responding with results."""
        while(self.message_queue):
            m = self.message_queue.pop()
            print(f"--- Scanner {self.id}: Msg on interface {m.interface} ---\n{m.text}\n------------------")
            splitted = m.text.replace("\n",":").split(":")
            _, source, _, dest, _, cmd, _, value = splitted
            if self.id == "2.1":
                interface = "S"
            else:
                interface = "N"
            r = 0
            if cmd == "Boot":
                r = self.boot(value)
            if cmd == "Aim":
                r = self.aim(value)
            if cmd == "Scan":
                r = self.scan(value)
            # respond with results
            #     Source:1.1
            #     Dest:2.1
            #     Command:Boot
            #     Value:13
            response = f"Source:{self.id}\nDest:{source}\nCommand:Result\nValue:{r}"
            self.send_message(response, interface)



### Beta Testing

No bugs found!