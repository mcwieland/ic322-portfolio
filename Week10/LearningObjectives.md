Michael Wieland
256828

## I can explain what autonomous systems are and their significance to the Internet.

Autonomous Systems are essentially groupings of routers that all fall under the same umbrella of adminstative control. They have two types of communication, intra-AS and inter-AS communication. Intra-AS communication is communication within a AS, some common system routing protocol is OSPF. Inter-AS communication is when an AS communicates outside of its AS, a common protocol is BGP. Each AS is assigned a unique ASN (Autonomous System Number). AS help solve problems with the internet's scaling and administative problems. By splitting the internet up, it become much more managable and navigatable. ISPs or organizations are able to use AS' to organize, provide security, and reduce the scale of their networks.

![Markdown Toggle](<LG_1.png>)

## I can describe how the BGP protocol works as well as why and where it is used.

The BGP (Border Gateway Protocol) is an inter-AS routing protocol that is used as the common inter-AS protocol between all routers. It is able to send data between thousands of ISPs and routers, so they can freely communicate with each other. It is a decentralized, asynchonous protocol that uses distance-vector routing. Instead of routing packets to specific addresses, BGP routes to CIDRized prefixes that represent a subnet or a group of subnets. These prefixes could be associated with different autonomous systems. BGP ensures that all routers across the Internet know about every subnet, providing prefix reachability information to neighboring autonomous systems.

![Markdown Toggle](<LG_2.png>)

We have ASs AS1, AS2, and AS3. They are connected through BGP, specifically eBGP. BGP comes in two different types, eBGP (external BGP) that connects routers in different ASs and iBGP (internal BGP) that connects routers in the same AS. Using a combination of these BGP types, all the routers seen here are connected and can route to any other.

## I can explain what the ICMP protocol is used for, with concrete examples.

The ICMP (Internet Control Message Protocol) allows hosts and routers to communicate network-layer information to each other. This is most offen used to error check. A router can send an ICMP message to the original host within an IP datagram. Here are some examples of common ICMP types and codes:

![Markdown Toggle](<LG_3.png>)