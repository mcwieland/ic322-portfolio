Michael Wieland
256828


R1. What is the difference between a host and an end system? List several different
types of end systems. Is a Web server an end system?


Host and End System are used interchangably within the text. End systems are PCs, phones, mail servers, and essentially all objects that use the internet. Web severs are another end system because they run applications on the behalf of the users, therefore esentially becoming a host themself. 


R4. List four access technologies. Classify each one as home access, enterprise
access, or wide-area wireless access.


Dial-Up modem: home access
DSL: home access
3G and 4G: wide-area wireless access
100 Mbps switched Ethernet: enterprise access


R11. Suppose there is exactly one packet switch between a sending host and a
receiving host. The transmission rates between the sending host and the
switch and between the switch and the receiving host are R1 and R2, respectively.
Assuming that the switch uses store-and-forward packet switching,
what is the total end-to-end delay to send a packet of length L? (Ignore queuing,
propagation delay, and processing delay.)


Because we are ignoring queuing, propagation, and processing delays, we only have to worry about transmission delays. A transmission delay is calculated using the formula L/R, where L is the length of the medium and R and the rate at which the data is transmitted. In our situation we have to calculate 2 transmission delays, one from the sending host to the switch and another from the switch to the recieving host. When we plug the numbers from the problem into our formula, we get transmission delays of L/R1(sending to switch) and L/R2(switch to recieving) giving a total transission delay of L/R1 + L/R2.


R12. What advantage does a circuit-switched network have over a packet-switched network?
What advantages does TDM have over FDM in a circuit-switched network?


A circuit-switched network guarantees all avaialable bandwidth of a connection for the connected host. This reduces the queuing delays as only one host is able to send data over the link, rather than multiple hosts fighting over bandwidth in a packet-switched network.

TDM gives hosts certain time slots for certain hosts to use the bandwidth of the connection. This is a much simipler endevor than FDM because there would be no need to send data at different frequencies for different hosts. The hardware to change frequencies is not needed and the system is easier to understand.


R13. Suppose users share a 2 Mbps link. Also suppose each user transmits continuously
at 1 Mbps when transmitting, but each user transmits only 20 percent
of the time. (See the discussion of statistical multiplexing in Section 1.3.)
a. When circuit switching is used, how many users can be supported?


Because each user transmits continuously at 1 Mbps we have the capacity to support 2 users with a 2 Mbps link (1 Mbps each).


b. For the remainder of this problem, suppose packet switching is used. Why
will there be essentially no queuing delay before the link if two or fewer
users transmit at the same time? Why will there be a queuing delay if
three users transmit at the same time?


There will be essentially no queuing delay if two or fewer users transmit at the same time because the total bandwidth needed will be a maximum of 2Mbps(1 Mbps per connection). Because that is the total bandwidth of the link, there should be no problem transmitting 2 or less users at once.

However if we are transmitting 3 users at once, we could possibily run into queuing delays. The total bandwidth needed by 3 concurrent users is 3Mbps(1 Mbps per connection). This is 1Mbps greater than the total bandwidth of the connection link(2 Mbps) and would cause a queuing delay for 1Mbps worth of packets if all 3 users were to transmit at the same time.


c. Find the probability that a given user is transmitting.


The probability that each user is transmitting is 0.2(from the problem).


d. Suppose now there are three users. Find the probability that at any given
time, all three users are transmitting simultaneously.


To find the probablity of 3 out of 3 simultanous users, we have to multiply the probability of each individual user together. Doing (.2)^3 would result in 0.008. Which is equivelent to 0.8% of the total time.


R14. Why will two ISPs at the same level of the hierarchy often peer with each
other? How does an IXP earn money?


ISPs need to regularly send data to each other to complete requests made by hosts. When an ISP needs to send data to another ISP of an equal tier(for example a regional ISP sending data to another regional ISP), it needs to go through a high tier ISP(tier 1) as an intermediary because normally ISPs of the same tier(except tier 1) do not have direct connections with each other. To use a higher tier ISP, the lower tiers need by bandwidth. Peering ISPs of the same tier create this connection and bypass the need to use a higher tier ISP to transmit data between ISPs that are peered together. 

The actual physical connection between two peering ISPs is called the IXP. IXPs usally charges a small fee for sending data across the connection based on the amaount and bandwidth used. However, using IXPs is ually cheaper than using the higher tier ISPs.


R18. A user can directly connect to a server through either long-range wireless or
a twisted-pair cable for transmitting a 1500-bytes file. The transmission rates
of the wireless and wired media are 2 and 100 Mbps, respectively. Assume
that the propagation speed in air is 3 * 10^8 m/s, while the speed in the twisted pair 
is 2 * 10^8 m/s. If the user is located 1 km away from the server, what is
the nodal delay when using each of the two technologies?


Nodal delay = Transmission + Propagation

Wireless:
Transmission = L/R = (1500 * 8) / (2 * 10^6) = 0.006 seconds = 6ms
Propagation = d/s = 1000 / (3 * 10^8) = 0.000003 seconds = 0.003ms
Nodal Delay = 6ms + 0.003ms = 6.003ms

Wired:
Transmission = L/R = (1500 * 8) / (100 * 10^6) = 0.00012 seconds = 0.12ms
Propagation = d/s = 1000 / (2 * 10^8) = 0.000005 seconds = 0.005ms
Nodal Delay = 0.12ms + 0.005ms = 0.125ms


R19. Suppose Host A wants to send a large file to Host B. The path from Host A to Host
B has three links, of rates R1 = 500 kbps, R2 = 2 Mbps, and R3 = 1 Mbps.
a. Assuming no other traffic in the network, what is the throughput for the
file transfer?


The throughput is limited by the lowest rate in the system, which is 500 kbps.


b. Suppose the file is 4 million bytes. Dividing the file size by the throughput,
roughly how long will it take to transfer the file to Host B?


time = file size / throughput
time = 4,000,000 / 500,000 = 8 seconds


c. Repeat (a) and (b), but now with R2 reduced to 100 kbps.


100kbps is now the new throughput.
time = 4,000,000 / 100,000 = 40 seconds