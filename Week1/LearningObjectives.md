Michael Wieland
256828

I can explain the role that the network core plays vs the network edge.


The network core is known as the backbone network and is used to quickly transfer data to devices further away from the edge network. It has very high speeds and serves to be the main transmitter of data from network edge to network edge. 

The network edge is essecially the intermediary between the internet and the devices themselevs. Each device that uses the internet has to be connected to it somehow. This short connection from the individual device to the first node of the network core is called the network edge. Without the network edge, users and devices would be unable to access the main internet(network core) and would not be able to transmit or recieve any data.


I can compare different physical technologies used in access networks, including dial-up, DSL, cable, fiber, and wireless.


Dial-up modem technology uses the public telephone network and modems to access the internet. Similar to how you would expect a phone call to work, a dial-up connection just connects two computers instead of two phones. Over twisted-pair copper wires, dial-up technology can reach speeds up to 56 kbps. 

DSL, or Digital Subscriber Line, makes use of the local telephone company (telco) infrastructure to send data as well as telephone signals at the same time. The data is sent along the telephone lines as high-frequecy tones and converted back into usable data at the telco's central office and to a machine called DSLAM. These DSL connections happen over twisted-pair copper wires, similar to that of Dial-up connections, however they are much faster, up to a 52 Mbs downstream rate and a 16 Mbs upstream rate. 

Cable uses the existing cable television company's infrastructure to transmit data from numerous houses at the same time to one cable head end which can connect these homes to the internet. Coaxial cables are used to connect homes to the neighborhood level junctions in groups of 500 to 5000. Fiber optics are then used to transmit from the nieghborhood level junctions to the cable head end. Coaxial cables are similar to twisted-pair copper wires as they also have 2 wires, however they are instead concentric. This are guided shared mediums where many end systems can be connected to directly to the wire and end systems are still able to accuratly recieve the send data.

Fiber optics are a thin, flexible medium that connects pulses of light, each as a bit. Because light is used, the transmitting speeds are tremedous, up to hundreds of gigabits per second. Beacuse they are so fast, they have developed into the prefered long-haul guided transmission media, esepcially for transcontinential. The only downside is the finacial cost associated with creating the optics and the equipment that they rely on, making many areas resistant to a full change to fiber optics. 

Terrestial Radio Channels make use of cell towers to allow devices to connect to the internet without a need for a physical connection. This makes them particularly attractive for small personal devices that we carry on our person(away from a cord). Many things affect the strength of cell towers including, enviromental condiderations, multipath fading, and interferance. Cell towers have wide area radio channels and allow users to connect from many of the more populated and advanced regions of the world. 

When cell towers or physical connections are not available, a user's next best choice would be satellite radio channels. Communication satellites are far off the ground and have view of a large swath of earth's surface and any one point in time. Therefore, they are able to recieve and transmit data to users in regions that are otherwise unassessible. Because of the distance from the user, satellites have a significant propigation delay which can be reduced by using a closer satellite.


I can use queuing, transmission, and propagation delays to calculate total packet delay, I can describe the underlying causes for these delays, and I can propose ways to improve packet delay on a network.

As we learned in class, we can determine the total packet delay by adding up all of the applicable delays. When calculating these delays, it's important to go step by step and know the limitations of the system that you are working with.

Queuing delay: This delay takes place when we are sending multiple packets at the same time. When an arriving packet needs to be transmitted onto a link but the link is already busy transmitting another packet, the packet must wait in a place called the output buffer. The packets must wait in the buffer until the link is available to send that packet. The time that the packet must wait is called the Queuing delay. One way to reduce this delay would be to add extra routes that packets can take to get to their destination. 

Transmission delay: This delay is the time it takes to push all of the packet's bits into the link. This is usally very brief, on the order of micro to milliseconds. The rate is different for different types of media, for example, ethernet vs cable. This delay can be reduced by using better media, such as fast ethernet cables.

Propagation delay: This delay is the time that it takes for the packet to be transmitted over the wire. Depending on the medium of transport, the packets travel close to the speed of light and mostly depend on the actual distance between them. This delay can be reduced by shorting the distance between the connected nodes.


I can describe the differences between packet-switched networks and circuit-switched networks.


The main differenece between packet-switched networks and circuit-switched is the allotment of resources. In a circut-switched network, an entire channel is reserved for communication. This is mostly used for phone calls as it makes sense that data would always transmitted continuously. This would not make sense for data transfer as data is usally not consitantly sent, making it an ineffective choice.

In a packet-switched network, data is broken up into individual packets and sent when the link is next available. Unlike circut-switched networks, each host does not have a dedicated connection and is susceptible to slowdowns due to a lack of available bandwidth if there are a lot of hosts trying to use the connection. Most modern systems us packet-switched networks because it is vastly more effiecent. Network resources are only used when needed.


I can describe how to create multiple channels in a single medium using FDM and TDM.


TDM, or Time-Division Multiplexing, is a process of a circut-switched network that divides available connection by time slots. It essentially takes the total time of connection and divides them into frames of a fixed length. These frames are then further divided into time slots in which an individual host is able to access all connection within their time slot. Each frame, every host will be able to have one time slot worth of connection, however this could become ineffective if there are less hosts than time slots in a frame. This situation would leave a blank time slot without an assigned host, therefore leaving the connection unused.

FDM, or Frequency-Division Multiplexing, is similarly a process of a circut-switched network, but divides available bandwidth by frequencey instead of time. For example, a medium of 5GHz total bandwidth could be broken up into 5 equivelent frequencies of 1GHz each for 5 different hosts. This process will always allow consistant connection, albiet a slow one. This can also become ineffective if there are too many individual frequency bands when compared to the number of hosts, leaving a certain frequency of the total bandwidth unused.


I can describe the hierarchy of ISPs and how ISPs at different or similar levels interact.


Access ISPs: These are the local ISPs that handle the connection between your internet devices and the regional ISPs that are a "tier up". There are many more access ISPs than higher tiers, and each access ISP usally connects to multiple regional ISPs for redunancy.

Regional ISPs: These are the intermediaries between the access ISPs and tier 1 ISPs. They sell internet access to local ISPs and buy it from tier 1 ISPs. 

Tier 1 ISPs: There are only a handful of tier 1 ISPs in the world. They act as the backbone of the internet and manage all of the traffic across the internet. If a tier 1 ISP goes down, all the regional and access ISPs that rely on it for internet also go down.


I can explain how encapsulation is used to implement the layered model of the Internet.


Hosts use encapsulation and the five layers of the internet to successfully send data from host to host. Layers can only talk to the layers immidately above or below. When you start to send data, you start on the application(5th) layer of host A. This layer sends the message to layer 4. Layer 4 gives layer 3 this message along with header information like a port number. Layer 3 adds a destination port address when it is sent to layer 2. Layer 2 adds an error checking message and sends the finalized message to layer 1. Layer 1's job is to encode the message onto the wire and sends it to connected hosts.

The message is bounced from host to host until it reaches the destination IP address. Hosts know what message are to them and what they need to pass along based on the 3rd layer's addition of the the destination IP address. When the message finally arrives to the destination host, the message is recieved on the 1st layer. The 1st layer sends the message up to the 2nd which removes a header and uses the error checking to see if a mistake was made before sending it up to the 3rd layer. The 3rd layer checks the given IP in the message and makes sure it matches it's own and sens it up to the 4th layer. The 4th layer sends up both the payload and the header which is a port number to the 5th layer. Back up in the application layer of the last host of the chain, the 5th layer is able to display the message along with any information that came along with it such as a port number.