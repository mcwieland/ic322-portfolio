Michael Wieland
m256828
Help Recieved: Worked with Anuj Sirsikar to answer questions

Introduction:
This lab introduced us and gave us a basic understanding of Wireshark. We learned about the tools that it had available and how to use them. Using the Wireshark packet sniffer we were able to trace the GET and Response and collect information about each.

Process:
I didn't deviate from instructions. Used from Textbook.
http://gaia.cs.umass.edu/kurose_ross/wireshark.php

Questions:
1.	Which of the following protocols are shown as appearing (i.e., are listed in the Wireshark “protocol” column) in your trace file: TCP, QUIC, HTTP, DNS, UDP, TLSv1.2?

When looking at the whole trace file, I can see TCP, TLSv1.2, ICMPv6 and ARP protocols.

2.	How long did it take from when the HTTP GET message was sent until the HTTP OK reply was received? By default, the value of the Time column in the packet-listing window is the amount of time, in seconds, since Wireshark tracing began.  (If you want to display the Time field in time-of-day format, select the Wireshark View pull down menu, then select Time Display Format, then select Time-of-day.)

It looks like it took roughly 30ms.

3.	What is the Internet address of the gaia.cs.umass.edu (also known as www-net.cs.umass.edu)?  What is the Internet address of your computer or (if you are using the trace file) the computer that sent the HTTP GET message?

The IP address of gaia.cs.umass.edu is 128.119.245.12. The IP address of my computer is 10.0.0.44.

4.	Expand the information on the HTTP message in the Wireshark “Details of selected packet” window (see Figure 3 above) so you can see the fields in the HTTP GET request message. What type of Web browser issued the HTTP request?  The answer is shown at the right end of the information following the “User-Agent:” field in the expanded HTTP message display. [This field value in the HTTP message is how a web server learns what type of browser you are using.]

User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:84.0) Gecko/20100101 Firefox/84.0\r\n
The User-Agent shows me that I was using Mozilla Firefox when I made the GET request.

5.	Expand the information on the Transmission Control Protocol for this packet in the Wireshark “Details of selected packet” window (see Figure 3 in the lab writeup) so you can see the fields in the TCP segment carrying the HTTP message. What is the destination port number (the number following “Dest Port:” for the TCP segment containing the HTTP request) to which this HTTP request is being sent?

The destination port is port 80. 

6.	Print the two HTTP messages (GET and OK) referred to in question 2 above. To do so, select Print from the Wireshark File command menu, and select the “Selected Packet Only” and “Print as displayed” radial buttons, and then click OK.

See "WiresharkPrintedPackets.pdf" in Week1 folder.