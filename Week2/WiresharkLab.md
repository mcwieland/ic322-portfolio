# WIRESHARK LAB WEEK 2
Michael Wieland
Help Recieved: None

Introduction:
This lab continues our process of understanding the internet through wireshark, specifically focusing on HTTP messages through TCP. We look at HTTP response codes, GET/Response interaction, downloading HTML documents, and how a cache works. Doing this lab, gives us a wider view on teh internet as a whole.


Process:
I was able to follow the online steps at https://gaia.cs.umass.edu/kurose_ross/wireshark.php-. All of my questions and answers for Wireshark Lab 2, HTTP, Version 8.1 are below.


Questions:
1. Is your browser running HTTP version 1.0 or 1.1? What version of HTTP is the
server running?


The browser and server are both running HTTP version 1.1.


2. What languages (if any) does your browser indicate that it can accept to the
server?


My browser indicates that it accepts "en-US" which is US English. "en" and "q=.9" are also accepted.


3. What is the IP address of your computer? Of the gaia.cs.umass.edu server?


The IP address of my computer is 10.25.130.217. The gaia.cs.umass.edu server's IP address is 128.119.245.12.


4. What is the status code returned from the server to your browser?


The status code is 200, "OK", which means everything ran successfully.


5. When was the HTML file that you are retrieving last modified at the server?


This HTML file that I am retrieving was last modifified on Thursday, 31SEP2023 at 05:59:01 GMT.


6. How many bytes of content are being returned to your browser?


128 Bytes are being returned to my browser. We can see this under file data.


7. By inspecting the raw data in the packet content window, do you see any headers
within the data that are not displayed in the packet-listing window? If so, name
one. 


No. I do not see any extra headers that are not already displayed in the packet-listing window.


8. Inspect the contents of the first HTTP GET request from your browser to the
server. Do you see an “IF-MODIFIED-SINCE” line in the HTTP GET?


I do not see an "IF-MODIFIED-SINCE" line in HTTP GET request.


9. Inspect the contents of the server response. Did the server explicitly return the
contents of the file? How can you tell?


Yes. The server explicitly returned the contents of the file under the line-based text data. This is the result: 
<html>\n 
Congratulations.  You've downloaded the file \n
http://gaia.cs.umass.edu/wireshark-labs/HTTP-wireshark-file1.html!\n
</html>\n


10. Now inspect the contents of the second HTTP GET request from your browser to
the server. Do you see an “IF-MODIFIED-SINCE:” line in the HTTP GET? If
so, what information follows the “IF-MODIFIED-SINCE:” header?


No. I do not see an “IF-MODIFIED-SINCE:” line in the second HTTP GET request.


11. What is the HTTP status code and phrase returned from the server in response to
this second HTTP GET? Did the server explicitly return the contents of the file?
Explain.


The status code returned was 304, "NOT MODIFIED", the server failed to explicitly return the contents of the file. This is because we already requested the contents of the file and have it stored in the cache. Because the browser reconizes that it was not modified since then, we are able to reuse the file's contents that we have saved in the cache quickly, instead of taking more time to retrieve the whole thing from the server.


12. How many HTTP GET request messages did your browser send? Which packet
number in the trace contains the GET message for the Bill or Rights?


There was one GET request message from my browser. Packet number 30 contained the GET request.


13. Which packet number in the trace contains the status code and phrase associated
with the response to the HTTP GET request?


Packet 35 contains the status code and the response (200, OK).


14. What is the status code and phrase in the response?


The status code was 200, OK, which means that everything was successful.


15. How many data-containing TCP segments were needed to carry the single HTTP
response and the text of the Bill of Rights?


5 TCP segements were used to carry the HTTP response and text of the Bill of Rights.


16. How many HTTP GET request messages did your browser send? To which
Internet addresses were these GET requests sent?


My browser sent 3 HTTP GET request messages. They were to the addresses: 128.119.245.12, 178.79.137.164, and 10.25.133.39.


17. Can you tell whether your browser downloaded the two images serially, or
whether they were downloaded from the two web sites in parallel? Explain.


The images come from 2 different source ports. Because they were seperate requests, which happened in a order, they mjst have been downloaded sequentially.


18. What is the server’s response (status code and phrase) in response to the initial
HTTP GET message from your browser?


The response to the initial HTTP GET message from your browser is "401, unathorized".


19. When your browser’s sends the HTTP GET message for the second time, what
new field is included in the HTTP GET message? 


There is a new authorization field called "Authorization: Basic".