Michael Wieland
256828


R5: What information is used by a process running on one host to identify a process running on another host?


A process needs the IP address and a port number to identifiy a process running on another host. This is because an IP address is able to identify the host that has the process that we want to identify and the port number is used to identify what kind of process the other host is running.


R8: List the four broad classes of services that a transport protocol can provide. For each of the service classes, indicate if either UDP or TCP (or both) provides such a service.


1: Reliable Data Transfer, TCP
2: Throughput, UDP
3: Timing, UDP (sometimes), it can send data freely, but not recieve as easily
4: Security, TCP


R11: Why do HTTP, SMTP, and IMAP run on top of TCP rather than on UDP?


HTTP, SMTP, and IMAP are protocols which need to prioritize safety and reliability over speed. UDP sends packets without waiting for an acknowledgement of success which leads to the potential of out of order or dropped packets. However, TCP creates a 3-way handshake with both hosts and waits for all packets to be acknowledged, which gives these protocols the safety and reliability that they need to function properly.


R12: Consider an e-commerce site that wants to keep a purchase record for each of its customers. Describe how this can be done with cookies.


Cookies are a tool that companies can use to track user information. This is used extensively online, especially e-commerance sites. Cookies are usally stored on your local browser through the Set-cookie field of an HTTP message. In this case, a e-commerance site is able to assign an ID number to each user and save this to their end system, their browser. In addition to ID numbers, things such as past orders, what users have browesed for, and their credit card information can also be saved here as a cookie. When the user logs back into this e-commerance site, this information would then be available to the site.


R13: Describe how Web caching can reduce the delay in receiving a requested object. Will Web caching reduce the delay for all objects requested by a user or for only some of the objects? Why?


When you make an HTTP request, you are requesting the ISP go find the object and return a copy of it back to you and your browser. The HTTP request's first stop is to a web cache, usally geogaphically nearby. Web caches are intermidate points that can store a large amount of data in a cache. If this web cache that the request travels to has the web object available in its cache, then it can simply and quickly return that object to you. Otherwise, it must then contact the orgin server that houses that web object, recieve it, and store it in the web cache, and only then return it to you. We can see that if the web cache has the requested webobject available, then the process is much faster and efficent than otherwise.


R14: Telnet into a Web server and send a multiline request message. Include in the request message the If-modified-since: header line to force a response message with the 304 Not Modified status code.


telnet gaia.cs.umass.edu 80
GET /kurose_ross/header_graphic_book_8E_2.jpg HTTP/1.1
Host: gaia.cs.umass.edu 
If-modified-since: Sat, 10 Jul 2021 18:22:56 GMT

HTTP/1.1 304 Not Modified
Date: Mon, 10 Sep 2023 21:11:53 GMT
Server: Apache/2.4.6 (CentOS) OpenSSL/1.0.2k-fips PHP/7.4.33 mod_perl/2.0.11 Perl/v5.16.3
ETag: "351dc-5c6c8fa2dc15a"


R26: In section 2.7, the UDP server described needed only one socket, whereas the TCP server needed two sockets. Why? If the TCP server were to support n simultaneous connections, each from a different client host, how many sockets would the TCP server need?


UDP servers are connectionless. They create one sided communications and only send packets one way, from the sender to the reciever. UDP connections simiply just send data out and hope that its reaches its target host, this is why it is deemed unreiable. Therefore, UDP sockets just have to recieve all packets being sent, no matter what the host. This is different in TCP connections where after the 3-way handshake, a specific socket is created between the 2 hosts to ensure reliability and security. Because the reciever side needs to be prepared for another host to send packets as well, a new "welcoming" socket is created that is able to recieve and handle new TCP connections. For TCP, if there are N connections being made, there must be N + 1 sockets.