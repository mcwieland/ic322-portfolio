Michael Wieland
256828

## I can explain the problem that NAT solves, and how it solves that problem.

### The problem:
When we have a small office, home office (SOHO) subnet and the owner wants to connect multiple of those devices to the internet via a LAN, every IP-capable device must have an IP address. This implies that as the SOHO subnet expands, it should also expand the block of IP addresses being allocated to it. However, there are cases where these new IP addresses are already allocated to other devices or subnets. One solution to get around this problem is NATs.

### What NATs do:
NATs (Network Address Translation) are routers that translate private IP addresses (used to communicate within one subnet) into public IP addresses (used to communicate with one side inside the subnet and another side outside). Devices can communicate freely within their own subnet using their assigned private IP addresses. However, to communicate outside the subnet, the device must communicate with the subnet's NAT. The NAT logs the request into a NAT table and sends the data out into the wider internet. When the NAT recieves data, it can can send the message to the orginal device using this table. It acts, not as a router but, as a single device with a single public IP. This allows more public IP addresses to be available, by using private IPs within their subnet.

## I can explain important differences between IPv4 and IPv6.

IPv6 was created as a solution to the fact that the 32-bit IPv4 IP addresses were being used up at such an unsustainable rate that they were epected to run out by 2008-2018. IPv6 creatation began in the mid 1990s because of the massive amount of time required for a successful rollout. IPv6 was focused on building off and improving the existing IPv4 format.

Some of the bigest changes from IPV4 to IPV6:
- Expanded addressing capabilities
    - increases the size of the IP address to 128 bits
        - every grain of sand can be IP-addressable
    - anycast address
        - allows a datagram to be delievered to any one of a group of hosts

- A steamlined 40-byte header
    - number and size of fields now amount to a fixed lenth of 40-bytes
        - allows for a faster processing speed of the IP datagram

- Flow labeling
    - allows labeling of packets belonging to particular flows for which the sender requests special handling, used as a non-default quality of service or real-time service
    - able to differentiate among the flows

New fields in IPV6:
- Version
- Traffic class
- Flow Label
- Payload Length
- Next Header
- Hop Limit
- Source and Destination Address
- Data

Old fields that are now removed:
- Fragmentation/ reassembembly
- Header Checksum
- Options

![Markdown Toggle](<ipv6header.png>)

## I can explain how IPv6 datagrams can be sent over networks that only support IPv4.

Tunneling: 
This is where an IPv6 datagram can be sent over networks that only support IPV4, by entering the entire IPV6 datagram into the IPV4 datagram. This encapulates the IPV6 datagram and disguises it inside the IPV4. Since the IPV4 datagram is on the outside, it is sent over the IPV4 network like normal until it reaches a router which is IPV6 compatable. This router is then able to tell if the IPV4 datagram contains an IPV6 datagram through protocol number field. Since the network now allows for IPV6 traffic, the IPV6 datagram can be extracted and sent along its way to the next router(IPV6 capabilities). Doing this allows IPV6 traffic to travel through IPV4 only networks, further enchancing the capabilities and usefulness of IPV6.

![Markdown Toggle](<ipv6tunnel.png>)