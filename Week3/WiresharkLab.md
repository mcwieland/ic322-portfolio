# WIRESHARK LAB WEEK 3
Michael Wieland
Help Recieved: None

## Introduction
This lab introduced us to DNS querys and responses. Analyzing DNS gives us more insight on its capabilities and real life applications.

## Process:
I was able to follow the online steps at https://gaia.cs.umass.edu/kurose_ross/wireshark.php-. All of my questions and answers for Wireshark Lab 3, DNS, Version 8.1 are below.

## Questions:

### 1. Run nslookup to obtain the IP address of the web server for the Indian Institute of Technology in Bombay, India: www.iitb.ac.in. What is the IP address of www.iitb.ac.in
The IP address is 103.21.124.10.

### 2. What is the IP address of the DNS server that provided the answer to your nslookup command in question 1 above?
The DNS server has an IP address of 127.0.0.53.

### 3. Did the answer to your nslookup command in question 1 above come from an authoritative or non-authoritative server?
The previous answer came from a non-authoritative server.

### 4. Use the nslookup command to determine the name of the authoritative name server for the iit.ac.in domain. What is that name? (If there are more than one authoritative servers, what is the name of the first authoritative server returned by nslookup)? If you had to find the IP address of that authoritative name server, how would you do so?
These are the authoritative servers:

dns1.iitb.ac.in
dns2.iitb.ac.in
dns3.iitb.ac.in

To find the IP address you can run nslookup the first server as the argument.

### 5. Locate the first DNS query message resolving the name gaia.cs.umass.edu. What is the packet number in the trace for the DNS query message? Is this query message sent over UDP or TCP?
The first packet was sent over UDP (packet number 15).

### 6. Now locate the corresponding DNS response to the initial DNS query. What is the packet number in the trace for the DNS response message? Is this response message received via UDP or TCP?
The packet number is packet 17 and it was alos sent over UDP.

### 7. What is the destination port for the DNS query message? What is the source port of the DNS response message?
The destination port for the DNS query message is 53. The source port of the response is also 53. Port 53 is exclusivly used by DNS.

### 8. To what IP address is the DNS query message sent?
The DNS query is sent to IP address 75.75.75.75

### 9. Examine the DNS query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?
It has 1 question and 0 answers.

### 10. Examine the DNS response message to the initial query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?
It has 1 question and 1 answer.

### 11. The web page for the base file http://gaia.cs.umass.edu/kurose_ross/ references the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E_2.jpg , which, like the base webpage, is on gaia.cs.umass.edu. What is the packet number in the trace for the initial HTTP GET request for the base file http://gaia.cs.umass.edu/kurose_ross/? What is the packet number in the trace of the DNS query made to resolve gaia.cs.umass.edu so that this initial HTTP request can be sent to the gaia.cs.umass.edu IP address? What is the packet number in the trace of the received DNS response? What is the packet number in the trace for the HTTP GET request for the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E2.jpg? What is the packet number in the DNS query made to resolve gaia.cs.umass.edu so that this second HTTP request can be sent to the gaia.cs.umass.edu IP address? Discuss how DNS caching affects the answer to this last question.
The initial HTTP request can be found on packet 22. The packet number that resolved the DNS query is packet 15. The response is on packet 17. The packet number for the image object is on packet 205. There is no additional packet where the DNS query was resolved, because is is probably already being stored in the host's DNS resolver cache. Since it is already in the cache, the local DNS server doesn't need to make another request, it is able to use the same object that it has saved.

### 12. What is the destination port for the DNS query message? What is the source port of the DNS response message?
The destination port and source port for the response are 53.

### 13. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?
The IP is 75.75.75.75. Because this is the PCAP file, this is not my local DNS server, however it could be the local DNS server of the people who made this PCAP file.

### 14. Examine the DNS query message. What “Type” of DNS query is it? Does the query message contain any “answers”?
This is a type "A" DNS query, meaning that it maps an IP address to a hostname. In this query message there was no answers.

### 15. Examine the DNS response message to the query message. How many “questions” does this DNS response message contain? How many “answers”?
It had 1 question and 1 answer.

### 16. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?
The IP address is 75.75.75.75. This is the PCAp file, it very could possibily be the default local DNS server.

### 17. Examine the DNS query message. How many questions does the query have? Does the query message contain any “answers”?
It has one question and no answers.

### 18. Examine the DNS response message. How many answers does the response have? What information is contained in the answers? How many additional resource records are returned? What additional information is included in these additional resource records?
The response message has 3 answers and 3 additional answers. In the regular answers, you can find the domains of 3 authoritative servers belonging to umass.edu. In the additional info, there are the IP addresses of the 3 authoritative serevrs.