Michael Wieland
2565828


Review Questions: 


R16: Suppose Alice, with a Web-based e-mail account (such as Hotmail or Gmail), sends a message to Bob, who accesses his mail from his mail server using IMAP. Discuss how the message gets from Alice’s host to Bob’s host. Be sure to list the series of application-layer protocols that are used to move the message between the two hosts.


Alice sends a message to Bob using her user-agent and bob's email address. Alice's user agent the uses HTTP to send the message thorugh her mail server. The email is then sent to Bob's email server using SMTP. SMTP stands for Simple Mail Transfer Protocol, and just how it is named, is the primary way to send mail. After it is in Bob's email server, it can retrieved by Bob through IMAP or POP. 


R18: What is the HOL blocking issue in HTTP/1.1? How does HTTP/2 attempt to solve it?


HOL or Head of Line blocking is an issue where lots of information is sent over a single TCP connection. A larger object in the front will "block" smaller objects from loading, because in HTTP 1.1 packets must arrive in order or else they must be resent. This causes an issue decreasing the overall speed of loading webpages. 

In HTTP 1.1, this issue was made better through the creation of parallel TCP connections where smaller objects have the option to just take a different route in order to not create a bottleneck.

In HTTP 2, the creators worked around this making frames that hold smaller HTTP messages by breaking teh larger ones up. This allows multiple objects to be sent at the same time, and therfore allows smaller objects to be sent without the larger ones being fully loaded.


R24: CDNs typically adopt one of two different server placement philosophies. Name and briefly describe them.


Enter Deep: In this philosophy, server clusters are spread all over the world at access ISPs. This lets the CND servers be as close to the end users as possible which reduces the propagation delay and number of hops between the client and the information that the CND server is holding. Bevcause these servers are so spread out, they are much harder to maintain, but make up for that in speed for the user.


Bring Home: In this philosophy, server clusers are placed at IXPs. This is essentially the opposite of the Enter Deep philosophy as it increaese propagation delay and reduces the diffuculty to maintain because they ahve many of them situated at the same place. These server clusters will be on average farther from the user geographically than the other pilosophy's server clusters will be.


Problems section:


P16: How does SMTP mark the end of a message body? How about HTTP? Can HTTP use the same method as SMTP to mark the end of a message body? Explain.


SMTP uses a single period to denote the the end of a message. This is also known as "CRLF". HTTP uses the information from the header line "Content-Length" to know where the end of the message should be. HTTP can not use the method of a period, because it is possible for a period to appear in the message itself. However, a "CRLF" can be used to end an HTTP message, it just must be denoted as "\r\n" instead.


P18(a and b only):


a: What is a whois database?


A whois database is a collection of information about the individuals and organizations who own/manage domain names. It includes the contact and technical information and is collected at the time of registration.


b: Use various whois databases on the Internet to obtain names of two DNS servers. Indicate which whois databases you used.


On whois.com, I found a DNS server called "ns1.google.com" for google.com. On whois.is, I found a DNS server called "NS.FACEBOOK.COM" for facebook.com.


P20: Suppose you can access the caches in the local DNS servers of your department. Can you propose a way to roughly determine the webservers (outside your department) that are most popular among the users in your department? Explain


If we have access to the cache, we can determine what websites consistantly stay in the cache for the greatest amount of time. Everytime a website is visted, it is put into the cache. Therefore, this means that the popular websites will remain in the cache the longest beacuse more people are visting them. Using this logic, we would be able to roughly estimate the most popular websites.


P21: Suppose that your department has a local DNS server for all computers in the department. You are an ordinary user (i.e., not a network/system administrator). Can you determine if an external Web site was likely accessed from a computer in your department a couple of seconds ago? Explain


The dig command can be used to search for information about websites, however it also returns the amount of time that it takes to respond as well. This means that if the response time for the dig command is 0ms, then the website and its data is stored on the local DNS server meaning that it was recently accessed. If the response tiem is more than 0ms, then it means that the command counldn't find it on the local DNS server and therefore wasn't recently visted.