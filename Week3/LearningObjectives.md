I can explain how the DNS system uses local, authoritative, TLD, and root servers to translate a domain name into an IP address.


Types of servers:
1. Root Server
Root servers are on the top of the hierarcy of all servers. There is a about 400 of these worldwide, they are controlled in 13 groupings.

2. TLD (Top-Level Domain Server)
Generic domain name servers and country-code domain servers. Ex. .uk, .com, .org, .au. These are responsable in storing all information for the domain names, which have the same extention.

3. Authoritative DNS Server
These servers provide offical information about a specific zone or domain name. Stores DNS Records (some common ones are IPv4 and IPv6).

4. Local DNS Server
Every ISP has a local DNS server. The server stores IP adresses that will be provided to the host when the host connects with an ISP.

Using these servers, the DNS system is able to translate human readable domain names into machine readable IP adresses and using this to connect the host to the requested internet service.


I can explain the role of each DNS record type.


(Name, Value, Type, TTL)

Type = A:
Name is a hostname and Value is the IP address for the hostname. This is the standard hostname to IP address mapping.

Type = NS:
Name is a domain and Value is the hostname of an authoritative DNS server that knows to obtain the IP addresses for hosts in the domain. This essentially passes along the DNS query to the next node in the chain, getting the query one step closer to being resolved.

Type = CNAME:
Value is a canonical hostname for the alias hostname Name. This can provide querying hosts the canonical name of the hostname.

Type = MX:
Value is the canonical name of the mail server that has the alias Name. These records allow the hostnames of mail servers to have simple alises.


I can explain the role of the SMTP, IMAP, and POP protocols in the email system.

SMTP(Simple Mail Transfer Protocol):
The SMTP is resonsible for sending outgoing emails. It comunicates with the email provider's SMTP server to successful compose and send. When an email is sent, it does not go directly to the recipant's email server, instead it must go along "hops" through SMTP servers. The client SMTP has TCP establish to port 25 at the server SMTP.

IMAP(Internet Message Access Protocol):
The IMAP is responsible for allowing clients to access emails stored on their email server. IMAP specializes at maintaining a synconized status across multiple devices and instances, constantly updating things like organization of emails and read/unread statuses. IMAP usally uses port 143.

POP(Post Office Protocol):
The POP is another option that clients possess when trying to access the emails stored on their email server. Unlike IMAP, POP downloads a copy of the emails to the local device. Due to this aspect, there is no syncronization
like IMAP. Email folders and read/unread staus is not shared or updated between devices and instances. POP usally uses port 110.


I know what each of the following tools are used for: nslookup, dig, whois.


nslookup(Name Sever Lookup):
The nslookup tool queries DNS servers. It is primarly used to retrieve information about domain names such as IP addresses or mail service records. 

dig(Domain Information Groper):
The dig tool also queries DNS servers. It is more detailed and provide information at a greater depth than nslookup. It can be used to get various DNS record types and more detailed data.

whois:
The whois tool retreieves information about domain registrations and ownership. It can provide details like contact information, creation date, and registar.