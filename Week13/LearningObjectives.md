Michael Wieland
256828

## I can describe the role a switch plays in a computer network.

A switch is a device within the link layer whose job is to forward frames to connected interfaces. To hosts and routers in a network switches are invisible and it just seems like the host/router sends their information directly to a LAN. These switches have buffers, similar to routers, that can handle more input frames than their link capacity allows for.

There are 2 functions of switches: **Forwarding** and **Filtering**

**Forwarding**: This is where a switch takes a frame as input and decides to which connected interface to send the frame over. Uses a switch table.

**Filtering**: This is where a switch takes a frame as input and decides wheather to drop it or let it be forwarded. This is essentially a "screening" for the frames.

Switch table: This is used when a switch is forwarding to determine which interface to send the frame over. Stores the MAC address, switch interface, and time of entry.

There are 3 main advantages to using switches:
- Elimination of collisions
    - never transmit more than one frame on a segment
- Heterogenous Links
    - isolates links so can use different speed and mediums
- Management
    - can bypass non-functioning switches
    - provide more in-depth information to the network manager

## I can explain the problem ARP solves, how it solves the problem, and can simulate an ARP table given a simple LAN scenario.

Problem: There are 2 different types of addresses that we have to deal with. There are the IP addresses from the network layer and the MAC addresses from the Link layer. To properaly communicate between devices on the internet there is a need to translate them.

How it solves it: ARP (Address Resolution Protocol) is able to resolve IP addresses to a MAC address. ARP is only able to resolve IP address to MAC addresses for host and routers that exist on the same subnet. It does this using ARP tables which have the equivelent translations between the 2 addresses on it.

![Markdown Toggle](<LG_2.png>)
In this scenario, all of these hosts belong to the same subnet, therefore we can use ARP to translate between the MAC and IP addresses. If host A wanted to send something to host C, it would have to figure out the IP address of C in order to encapsulate the datagram correctly. To get the Host C's IP address, host A will have to look at its own ARP table stoed in its memory and use Host C's MAC address to find the associated IP address for that MAC address.

## I can explain CSMA/CA, how it differs from CSMA/CD, what problems it addresses, and how it solves them.

CSMA/CA (Carrier Sense Multiple Access with Collision Avoidance) is a multiple access protocol used to coordinate transmissions between devices using 802.11 MAC protocol. The main different between these two protocl versions is that CSMA/CA uses collusion avoidance rather than the collision dection in CSMA/CD. This means that when using CSMA/CA, collisions can occur but remained undected and the entire packet is transmitted reguardless. This is very different from CSMA/CD where the packet is stopped from transmitting as soon as collisions are detected. 

Problem: One problem is that the collisons impact the how the packets are transfered. This can be solved by making the reciving station send a response back confirming that teh apcket was sent and recieved without issue.

An example of this in use is WiFi. It is unreasonable for WiFi to use error detection because of the amounts of needed singals that it recieved. Therefore CSMA/CA is a much better protocol to use here.

Here are the steps of CSMA/CA:

- If initially the station sense the channel idle, it transmits its frame after a short period of time known as the Distributed Inter-frame Space (DIFS).
- Otherwise, the station chooses a random backoff value using the binary exponential backoff and counts down this value after DIFS when the channel is sensed idle.
- When the counter reaches zero the station transmits the entire frame and then waits for an acknowledgement.
- If an acknowledgement is received, the transmitting station knows that its frame has been correctly received at the destination station.