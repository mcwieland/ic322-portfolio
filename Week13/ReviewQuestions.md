Michael Wieland
256828

# Chapter 6:

# Review Questions: 

## R10: Suppose nodes A, B, and C each attach to the same broadcast LAN (through their adapters). If A sends thousands of IP datagrams to B with each encapsulating frame addressed to the MAC address of B, will C’s adapter process these frames? If so, will C’s adapter pass the IP datagrams in these frames to the network layer C? How would your answers change if A sends frames with the MAC broadcast address?

Yes. C's adapter will process these frames. However, since they addressed to B, C will drop these frames threfore not bringing it into the network layer. If A sends the frames with the MAC broadcast address, then the frames will not be dropped and sent ot the network layer.

## R11: Why is an ARP query sent within a broadcast frame? Why is an ARP response sent within a frame with a specific destination MAC address?

An ARP query is used to find a MAC address that matches the IP address in the query. It needs to be over the broadcast frame because we dont know exactly which node has what IP address. We are able to send it back with a specific destintaion MAC address because we know who sent out the ARP query and we also know what node matched the IP address(the one that it ended on).

# Problems section: 

## P14: Consider three LANs interconnected by two routers, as shown in Figure 6.33.

### a. Assign IP addresses to all of the interfaces. For Subnet 1 use addresses of the form 192.168.1.xxx; for Subnet 2 uses addresses of the form 192.168.2.xxx; and for Subnet 3 use addresses of the form 192.168.3.xxx.
### b. Assign MAC addresses to all of the adapters.

![Markdown Toggle](<RQP14.jpg>)

### c. Consider sending an IP datagram from Host E to Host B. Suppose all of the ARP tables are up to date. Enumerate all the steps, as done for the single-router example in Section 6.4.1.

1) E creates IP datagram with destination B
2) E encapsulates the frame from step 1 with its destination set to MAC address of R2
3) R2 receives frame, sends to MAC address of R1
4) R1 removes datagram and re-encapsulates the frame with B's MAC address for the destination
5) R1 creates link layer frame and forwads it to B
6) B receives the frame

### d. Repeat (c), now assuming that the ARP table in the sending host is empty (and the other tables are up to date).

1) E firsts creates its IP datagram with destination to B
2) E encapsulates the frame from step 1 with its destination set to MAC address of R2
3) ARP query reaches R2, R2 then responds to E
4) E receives the ARP response and updates the ARP table
5) E encapsulates IP datagram to B, with the MAC address to R2
6) E sends the Ethernet frame to its LAN
7) The LAN follows the steps in part C

## P15:

### a. Consider sending an IP datagram from Host E to Host F. Will Host E ask router R1 to help forward the datagram? Why? In the Ethernet frame containing the IP datagram, what are the source and destination IP and MAC addresses?

No. Host E will not ask router R1 to help forward the datagram. This is because F is within the same subnet. The destination should be Host F's IP and MAC addresses and source will be Host E's.

### b. Suppose E would like to send an IP datagram to B, and assume that E's ARP cache does not contain B's MAC address. Will E perform an ARP query to find B's MAC address? Why? In the Ethernet frame containing the IP datagram destined to B that is delivered to router R1, what are the source and destination IP and MAC addresses?

No. E will not perform an ARP query to find B's MAC address. E will send an ARP query to R1. The source will be Host E's addresses. The destination will be Host B for the IP and R1 for the MAC.

### c. Suppose Host A would like to send an IP datagram to Host B, and neither A’s ARP cache contains B’s MAC address nor does B’s ARP cache contain A’s MAC address. Further suppose that the switch S1’s forwarding table contains entries for Host B and router R1 only. Thus, A will broadcast an ARP request message. What actions will switch S1 perform once it receives the ARP request message? Will router R1 also receive this ARP request message? If so, will R1 forward the message to Subnet 3? Once Host B receives this ARP request message, it will send back to Host A an ARP response message. But will it send an ARP query message to ask for A’s MAC address? Why? What will switch S1 do once it receives an ARP response message from Host B?

The ARP request will be dropped by S1 because it know that it came from the saem interafce that the MAc address is on. Therefore R1 does not recieve the ARP query. Additionally, Host B does not send an ARP message to ask for A's MAC address because the source address of the datagram is A's MAC. Since they do not match, the datagram will be dropped.

# Chapter 7:

# Review Questions:

## R3: What are the differences between the following types of wireless channel impairments: path loss, multipath propagation, interference from other sources?

Path loss: When the distance between the sender and the reciever increaes, the signal becomes weaker.
Multipath propagation: When the signal is bounced/reflected off things, the final signal is blurred because some data overlaps.
Interference: Multiple sources transmitting over teh same frequenceies will mix the signal and interfer with each other.

## R4: As a mobile node gets farther and farther away from a base station, what are two actions that a base station could take to ensure that the loss probability of a transmitted frame does not increase?

The base station can either increase its power or slow the transmission rate.

# Problems section:

## P6: In step 4 of the CSMA/CA protocol, a station that successfully transmits a frame begins the CSMA/CA protocol for a second frame at step 2, rather than at step 1. What rationale might the designers of CSMA/CA have had in mind by having such a station not transmit the second frame immediately (if the channel is sensed idle)?

Because CSMA/CA is made to transmit frame by frame, another source maybe be sending a frame right after the first one ends. By waiting and not sending the next frame immediatly, it helps wait for any potential propagation delay to be over and make sure there are no collisions.

## P7: Suppose an 802.11b station is configured to always reserve the channel with the RTS/CTS sequence. Suppose this station suddenly wants to transmit 1,500 bytes of data, and all other stations are idle at this time. As a function of SIFS and DIFS, and ignoring propagation delay and assuming no bit errors, calculate the time required to transmit the frame and receive the acknowledgment.

If all other stations are silent and not sending any data themselves, then the time required will just be DIFS + SIFS.