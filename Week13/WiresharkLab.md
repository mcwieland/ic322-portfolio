Michael Wieland
256828
# WIRESHARK LAB WEEK 3

## Introduction: This wireshark lab was about ARP and Ethernet protocols.

## Collaboration: None

## Process: I used the intructions found on the wireshark document and the PCAP file.

## Questions:

### 1. What is the 48-bit Ethernet address of your computer?

The address is 00:09:5b:61:8e:6d.

### 2. What is the 48-bit destination address in the Ethernet frame? Is this the Ethernet address of gaia.cs.umass.edu? (Hint: the answer is no). What device has this as its Ethernet address? [Note: this is an important question, and one that students sometimes get wrong. Re-read pages 468-469 in the text and make sure you understand the answer here.]

The destinaiton address is 00:0c:41:45:90:a8.

### 3. Give the hexadecimal value for the two-byte Frame type field. What upper layer protocol does this correspond to?

The hex value is 0x0800. This is the IP protocol.

### 4. How many bytes from the very start of the Ethernet frame does the ASCII “G” in “GET” appear in the Ethernet frame?

The ASCII "G" is 52 bytes from the start of the frame.

### 5. What is the value of the Ethernet source address? Is this the address of your computer, or of gaia.cs.umass.edu (Hint: the answer is no). What device has this as its Ethernet address?

The source is 00:0c:41:45:90:a8. This is from gaia.cs.umass.

### 6. What is the destination address in the Ethernet frame? Is this the Ethernet address of your computer?

The destination is 00:09:5b:61:8e:6d. This is my computer.

### 7. Give the hexadecimal value for the two-byte Frame type field. What upper layer protocol does this correspond to?

The hex value is 0x0800. This is the IP protocol.

### 8. How many bytes from the very start of the Ethernet frame does the ASCII “O” in “OK” (i.e., the HTTP response code) appear in the Ethernet frame?

The ASCII "O" is 52 bytes from the start of the frame.

### 9. Write down the contents of your computer’s ARP cache. What is the meaning of each column value?

Unable to do with PCAP file.

### 10. What are the hexadecimal values for the source and destination addresses in the Ethernet frame containing the ARP request message?

The source is 00:d0:59:a9:3d:68.
The destination is ff:ff:ff:ff:ff:ff.

### 11. Give the hexadecimal value for the two-byte Ethernet Frame type field. What upper layer protocol does this correspond to?

The hex value is 0x0806. This is the ARP protocol(Ethertype).

### 12. Download the ARP specification from ftp://ftp.rfc-editor.org/in-notes/std/std37.txt. A readable, detailed discussion of ARP is also at http://www.erg.abdn.ac.uk/users/gorry/course/inet-pages/arp.html.

### a. How many bytes from the very beginning of the Ethernet frame does the ARP opcode field begin?

It begins 24 bytes from the start of the frame.

### b. What is the value of the opcode field within the ARP-payload part of the Ethernet frame in which an ARP request is made?

It is 0x0001 which means request.

### c. Does the ARP message contain the IP address of the sender?

Yes it does contain it.

### d. Where in the ARP request does the “question” appear – the Ethernet address of the machine whose corresponding IP address is being queried?

The recievers MAC address.

### 13. Now find the ARP reply that was sent in response to the ARP request.

### a. How many bytes from the very beginning of the Ethernet frame does the ARP opcode field begin?

It begins 24 bytes from the start of the frame.

### b. What is the value of the opcode field within the ARP-payload part of the Ethernet frame in which an ARP response is made?

It is 0x0002 whihc means reply

### c. Where in the ARP message does the “answer” to the earlier ARP request appear – the IP address of the machine having the Ethernet address whose corresponding IP address is being queried?

The senders MAC address.

### 14. What are the hexadecimal values for the source and destination addresses in the Ethernet frame containing the ARP reply message?

The source hex value is 00:06:25:da:af:73. An the destination hex value is 00:d0:59:a9:3d:68.

### 15. Open the ethernet-ethereal-trace-1 trace file in http://gaia.cs.umass.edu/wireshark-labs/wireshark-traces.zip. The first and second ARP packets in this trace correspond to an ARP request sent by the computer running Wireshark, and the ARP reply sent to the computer running Wireshark by the computer with the ARP-requested Ethernet address. But there is yet another computer on this network, as indicated by packet 6 – another ARP request. Why is there no ARP reply (sent in response to the ARP request in packet 6) in the packet trace?

The replay is send directly to the sender's Ethernet address. Because there wasn't a reply, that must mean that we weren't the sender.