Michael Wieland
256828

# Review Questions: 

## R5: What is the “count to infinity” problem in distance vector routing?

The count to infinity problem occurs when 2 adjacent nodes think that the shortest path to another node it through each other. This is impossible without a mistake when calcuating the shortest route. This type of mistake creates a routing loop.

Because each node thinks that the shortest path is through the other, the link cost will keep increasing by the weight of the edge between them. This will continue until the link cost is larger than the mistake which will be enough to break out of the loop.

## R8: True or false: When an OSPF route sends its link state information, it is sent only to those nodes directly attached neighbors. Explain.

False. Because OSPF is a link state algorithm, it floods the entire AS with its routing information.

## R9: What is meant by an area in an OSPF autonomous system? Why was the concept of an area introduced?

Areas are a hiearchy in an AS. Each area has its own link-state algorithm and they share with all other outers in the area. Border routers route between other areas.

# Problems section: 

## P3: Consider the following network. With the indicated link costs, use Dijkstra’s shortest-path algorithm to compute the shortest path from x to all network nodes. Show how the algorithm works by computing a table similar to Table 5.1.

![Markdown Toggle](<P3.JPG>)

## P4: Consider the network shown in Problem P3. Using Dijkstra’s algorithm, and showing your work using a table similar to Table 5.1, do the following (only nodes v,y, and w):

![Markdown Toggle](<P4v.JPG>)

![Markdown Toggle](<P4y.JPG>)

![Markdown Toggle](<P4w.JPG>)

## P5: Consider the network shown below, and assume that each node initially knows the costs to each of its neighbors. Consider the distance-vector algorithm and show the distance table entries at node z.

![Markdown Toggle](<P5.JPG>)

## P11: Consider Figure 5.7. Suppose there is another router w, connected to router y and z. The costs of all links are given as follows: c(x,y) = 4, c(x,z) = 50, c(y,w) = 1, c(z,w) = 1, c(y,z) = 3. Suppose that poisoned reverse is used in the distance-vector routing algorithm.

### a: When the distance vector routing is stabilized, router w, y, and z inform their distances to x to each other. What distance values do they tell each other?

Router y will say 4, router w will say infinity and router z will say infinity.

### b: Now suppose that the link cost between x and y increases to 60. Will there be a count-to-infinity problem even if poisoned reverse is used? Why or why not? If there is a count-to-infinity problem, then how many iterations are needed for the distance-vector routing to reach a stable state again? Justify your answer.

There will not be a count to infinity problem because the updated values will be used. Router y will send to routers w and z which will also update. 

### c: How do you modify c(y,z) such that there is no count-to-infinity problem at all if c(y,x) changes from 4 to 60?

c(y,z) could be made to any number that makes it so z's path is not through router x. This way we do not even need to consider router x.