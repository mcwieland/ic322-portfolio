Michael Wieland
256828

## I can describe how Link State algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Link State algorithm.

Link State routing is a dynamic routing algorithm where each router shares knowledge of its neighbors with every other router in the network. This algorithm makes use of Dijkstra's algorithm in making routing tables. Dijkstra's algorithm finds the shortest path between a source node and every other node in the network by using the weights on the edges between the nodes to determine the shortest path. There is a different routing table for every node because each is tailored for the starting node. It also provides  a greater degree of detail than Distance Vector algorithm routing tables. Unlike Distance vector algorithms, information is only shared when there is a change. 

### Some common problems and some solutions:

- Heavy traffic
    - Updates are sent to every node in the network every time a distance is changed
- Infinite looping
    - Can be solved by using the Time to Live (TTL) field

## I can describe how Distance Vector algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Distance Vector algorithm.

Distance Vector routing is a dynamic routing algorithm where each router computes a distance between itself and each possible destination (it’s neighbors). This algorithm uses the Bellman-Ford equation to make routing table that calculate the distance to every node from every node using the information that it recieves from it's neighbors. Every node determines the distance to every neighbor and sends that information to each neighbor in a single "step". These steps occur on fixed intervals and will eventually allow every node to have knowledge about every other in the network, provided they have enough time to make the steps to transverse the whole thing. It can even make routing tables when the distance between nodes is negative. 

### Some common problems and some solutions:

- Count to infinity problem
    - Solved by splitting horizon, with poison reverse technique
- Only sharing with neighbors
    - Distance vector routing only contacts its neighbors (like a game of telephone)
- Persistent looping (with negative value loops)
    - A loop with a negative overall value will keep looping in order to get the shortest path possible
    - can use Time to Live field
