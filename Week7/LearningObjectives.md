Michael Wieland
256828


## I can explain what a subnet is, how a subnet mask is used, and how longest prefix matching is used to route datagrams to their intended subnets.

Subnet: In simple terms, a subnet is a network inside of a network. They makes networks more effiecent by reducing the amount of uneeded network hops when transmitting network traffic. Organizations use the process of subnetting to gain this effiecency advantage and make it more manageable. 

Subnet Mask: All IP addresses in a subnet share a number of bits in the prefix. A subnet mask determines what and how many shared bits there are. In an example of (255.255.255.0/20), the "/20" tells us that the first 20 bits of the IP address are shared by all in that subnet. Therefore a larger subnet mask number is a small subnet and a smaller subnet mask number is a larger subnet.

Longest Prefix Matching: When we encounter a destination IP address, it is possible that it could match to 2 or more subnets. The longest prefix matching rule tells us that the longest matching entry in a forwarding table is the correct match to use. For example, if two subnets matched to an IP with the subnet mask numbers of 20 and 24, the sebnet associated with teh subnet mask 24 would be the correct subnet to use because more of the prefix matches with the destination IP.

## I can step though the DHCP protocol and show how it is used to assign IP addresses.

DHCP(Dynamic Host Configuration Protocol) is used to supply IP addreses to a host automatically. Depending on the current DHCP configuration, these supplied IP addresses can either be the same or a different temporary address for each connection. 

DHCP Server Discovery: In this step, the host attempts to find DHCP servers to interact with. It sends a DHCP discover message with UDP and a destination IP of 255.255.255.255(broadcast IP) and a host IP of 0.0.0.0. This allows the host to send this discover message to all nodes on the subnet to tell them that they are looking for a DHCP server.

Server Offer: When a DHCP sever encounters a DHCP discovery message, it then it responds with a DHCP offer message. This message is also broadcasted on the broadcast destination IP(255.255.255.255), because the host does not have an IP address to connect to. Multiple DHCP offer messages to a single DHCP discovery message result in multiple offer (choices) for the host to choose from. The offer contains a proposed IP for the host.

DHCP Request: The host must choose one of the offers sent by the DHCP servers. After choosing, the host sends a DHCP request message, with the same configuration parameters as the chosen offer.

DHCP ACK: After receiving a request message with the same configuration parameters as the given offer, the server sends a DHCP ACK back to the host. After the host recieves this, the host can use the DHCP-allocated IP address for the lease duration.

![Markdown Toggle](<DHCP_example_IP.png>)