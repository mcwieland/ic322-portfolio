Michael Wieland
256828

# Review Questions:

## R11: Describe how packet loss can occur at input ports. Describe how packet loss at input ports can be eliminated (without using infinite buffers).

When packets arrive at ports, they must wait to be processed. They wait in buffers (quenues) and will remain there until processed. If too many packets pile up in the buffer, the buffer will become full. With no space to add any additional incomming packets, the packets that cannot fit, will be dropped. This can be solved by making the switch fabric speed as many times as fast as the input line speed as the number of input ports.

## R12: Describe how packet loss can occur at output ports. Can this loss be prevented by increasing the switch fabric speed?

Packets can only be transmitted one at a time. As packets come through the switch fabric, they wait to be transmitted in the output buffer. This buffer has the potential to fill up and drop packets that cannot fit. If the switch fabric speed is increased, then more packets will line up in the output buffer, making the problem even worse.

## R13: What is HOL blocking? Does it occur in input ports or output ports?

HOL (head-of-the-line) blocking occurs in input ports. This is where packets are blocked from getting to their output ports because a packet in the front of the queue is not ready yet. This casues significant packet loss because it makes it easy for the buffer to be overflown.

## R16: What is an essential different between RR and WFQ packet scheduling? Is there a case (Hint: Consider the WFQ weights) where RR and WFQ will behave exactly the same?

In RR (Round Robin) scheduling, packets are organized into classes. The scheduler visits each class to prevent packet starvation. In WFQ (Weighted Fair Queuing) scheduling, the same process as Round Robin occurs, but each class is assigned a priority (weight). When the wieght assigned to each class is the same then RR and WFQ are identical.

## R18: What field in the IP header can be used to ensure that a packet is forwarded through no more than N routers?

The Time-to-live header ensures that a packet is not fowarded forever. Every time the packet is processed by a router, the value in this field decreases. If this value reaches 0, the packet will be dropped. Therefore setting the value to N will make sure less than or equal to N routers will be traveled through.

## R21: Do routers have IP addresses? If so, how many?

Yes, routers need IP addresses to recieve and send datagrams. IP addresses are associated with different interfaces on the router that is connected to a host. So, that means that a router has the same number of IP addresses that it has connections to other hosts.

# Problems Section:

## P4: Consider the switch shown below. Suppose that all datagrams have the same fixed length, that the switch operates in a slotted, synchronous manner, and that in one time slot a datagram can be transferred from an input port to an output port. The switch fabric is a crossbar so that at most one datagram can be transferred to a given output port in a time slot, but different output ports can receive datagrams from different input ports in a single time slot. What is the minimal number of time slots needed to transfer the packets shown from input ports to their output ports, assuming any input queue scheduling order you want (i.e., it need not have HOL blocking)? What is the largest number of slots needed, assuming the worst-case scheduling order you can devise, assuming that a non-empty input queue is never idle?

The best case scenario would require 3 time slots:

1 for x, y, and z


The worst case scenario would require 5 time slots(one for each packet):

1 for x.
2 for y and z.


## P5: Suppose that the WFQ scheduling policy is applied to a buffer that supports three classes, and suppose the weights are 0.5, 0.25, and 0.25 for the three classes.

### a: Suppose that each class has a large number of packets in the buffer. In what sequence might the three classes be served in order to achieve the WFQ weights? (For round robin scheduling, a natural sequence is 123123123 . . .).

One possible sequence would be, 1213121312... This is because the first class has the most weight and the second and third have the same. It would go first, second, and then third.

### b: Suppose that classes 1 and 2 have a large number of packets in the buffer, and there are no class 3 packets in the buffer. In what sequence might the three classes be served in to achieve the WFQ weights?

One possible configuration is 112112112... This is where the first class is used double that of the second.

## P8: Consider a datagram network using 32-bit host addresses. Suppose a router has four links, numbered 0 through 3, and packets are to be forwarded to the link interfaces as follows:

### a: Provide a forwarding table that has five entries, uses longest prefix matching, and forwards packets to the correct link interfaces.

Prefix Match | Interface
-------------|----------
11100000 00 | 0
11100000 01000000 | 1
11100000 | 2
11100001 0 | 2
otherwise | 3

### b: Describe how your forwarding table determines the appropriate link interface for datagrams with destination addresses:

1st destination address: otherwise, goes to link 3.
2nd destination address: matches 11100001 0, goes to link 2.
3rd destination address: matches 1110000, goes to link 2.

## P9: Consider a datagram network using 8-bit host addresses. Suppose a router uses longest prefix matching and has the following forwarding table: (For each of the four interfaces, give the associated range of destination host addresses and the number of addresses in the range.)

Interface 0:
Range: 00000000-00111111, 2^6 or 64 addresses available

Interface 1:
Range: 01000000-01111111, 2^5 or 32 addresses available

Interface 2:
Range: 01100000-01111111 and 10000000-10111111, 2^6+2^5 or 96 addresses available

Interface 3:
Range: 11000000-11111111, 2^6 or 64 addresses available

## P11: Consider a router that interconnects three subnets: Subnet 1, Subnet 2, and Subnet 3. Suppose all of the interfaces in each of these three subnets are required to have the prefix 223.1.17/24. Also suppose that Subnet 1 is required to support at least 60 interfaces, Subnet 2 is to support at least 90 interfaces, and Subnet 3 is to support at least 12 interfaces. Provide three network addresses (of the form a.b.c.d/x) that satisfy these constraints.

Subnet 1: 60 interfaces -> round up to 64 -> need 6 bits for host I.D. (2^6 = 64)
Subnet 2: 90 interfaces -> round up to 128 -> need 7 bits for host I.D. (2^7 = 128)
Subnet 3: 12 interfaces -> round up to 16 -> need 4 bits for host I.D. (2^4 = 16)
There are 32 bits in an IP address.
32-6 = 26 bits for subnet
32-7 = 25 bits for subnet
32-4 = 28 bits for subnet
Three possible addresses satisfying the above constraints are as follows:
Subnet 1: 223.1.17.128/26
Subnet 2: 223.1.17.0/25
Subnet 3: 223.1.17.192/28