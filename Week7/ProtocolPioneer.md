Michael Wieland
256828

# Protocol Pioneer Chapters 1 and 2

## Introduction
This week I am doing LCDR Down's game, Protocol Pioneer. This game tries to teach people about computer networks, when they may have not been formally taught. We are some of the first players and are trying to find bugs in the code.


## Collaboration
No major collaboration with solutions. Talked about the game in general in class.


## Process
Followed the prescribed steps on github to download and launch the game. Started with chapter 1 and continued to chapter 2.


## Chapter 1

Chapter 1 was an introduction to the game and the console that we would interact with. Using python, we would code in the JupiterLab text editor to actually inpact what goes on in the chapter's simulation, trying to successfully win the game. In chapter 1, we needed to communicate with the mothership in order to recieve help repairing our ship.

### Strategy
In Chapter 1, I needed to change a parameter in the self.send_message() line to say "Message Recieved" and the interface "W".

    # Welcome to the StellarScript Console. This is where you will program your
    # communication strategy.
    #
    # Once every "tick", the `process_strategy()` function is called. During this function call,
    # you should:
    # 1. Check for messages that you received since the last tick
    # 2. Send any messages you want to send.
    # 3. Keep track of any state

    def process_strategy(self):

        # You have a `self.message_queue()` variable that you can access. It's a simple
        # Python List. Each element has a `text` and `interface` element.
        # Note that there is a one-tick processing delay: a message will arrive at your ship,
        # it will disappear (it is being processed), and the next tick it will arrive in
        # your queue.
        while(self.message_queue):
            m = self.message_queue.pop()
            print(f"Msg on interface {m.interface}: {m.text}")

        # You also have a `self.state` variable available to use. This is useful if you want
        # to remember things between ticks. `self.state` is a Python Dict with string keys.
        if "ticks" not in self.state:
            self.state["ticks"] = 1
        else:
            self.state["ticks"] += 1
        print(f"Tick {self.state['ticks']}...")

        # You can send a message using the `self.send_message()` function. The
        # function takes 2 string arguments: `message` and `interface`.
        # `message` is the text of the message and `interface` is either "N", "S", "E", or "W".
        # The value of `interface` determines which direction the message will be
        # sent out on. "N" is the northern interface, "S" is the southern interface, etc.
        if self.state["ticks"] % 2 == 0:
            self.send_message("Message Received", "W")


    # You can set `wait_for_user` to be True if you want the game to pause every tick.
    s = Scenario1(process_strategy, wait_for_user=False)
    s.run()


### Beta Testing

No bugs found

## Chapter 2
After contacting the mothership, repair ships were sent to help out with the repairs on our ship. We were given the task to guide them in.

### Strategy
Chapter 2 created a situation where we had to accept 3 numbers at different times, from different sources. We then had to return the sum of the numbers we recieved the 3rd number from a source. I saved the number of transmissions that I recieved for each interface as the first number of the interface list and the sum of the number as the second number in the interface list.

        # Welcome back to the StellarScript Console!

    def process_strategy(self):

        # Initialize your state variable
        if "received" not in self.state:
            # You can use this value to keep track of the messages you get.
            # You can store the total message count as self.state["received"][interface][0],
            # and the sum as self.state["received"][interface][1]. If you want.
            self.state["received"] = {
                "N": [0,0],
                "S": [0,0],
                "E": [0,0],
                "W": [0,0]
            }
        
        while(self.message_queue):
            m = self.message_queue.pop()
            print(f"Msg on interface {m.interface}: {m.text}")
            self.state["received"][m.interface][0] += 1
            self.state["received"][m.interface][1] += int(m.text)
            #only after I get at least 3 messages will I send one back
            if self.state["received"][m.interface][0] == 3: 
                self.send_message(str(self.state["received"][m.interface][1]), m.interface)


    # Remember, you can set `wait_for_user` to be True if you want the game to pause every tick.
    s = Chapter2(process_strategy, wait_for_user=False)
    s.run()


### Beta Testing

No bugs found